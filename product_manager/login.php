<?php
  include '../connect.php';
  session_start();
  session_destroy();

  if (!empty($_SESSION["userinfo_ID"]) && ($_SESSION["user_role_ID"] == 1))
  {
    jsRedirect("replace", "index.php");
    exit(0);
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST")
  {
    $_SESSION["login_role_ID"] = "";
    $input_username = $_POST["username"];
    $input_password = md5($_POST["password"]);
    $sql_get_login = "SELECT login_userinfo_ID, login_password, login_status, login_role_ID FROM tb_login WHERE login_username = '$input_username' ";
    $result_get_login = mysqli_query($con, $sql_get_login);
    
    if ($result_get_login->num_rows > 0)
    {
      if ($row = $result_get_login->fetch_assoc())
      {
        $db_password  = $row["login_password"];
        $db_status  = $row["login_status"];
        $db_userinfo_ID = $row["login_userinfo_ID"];
        $_SESSION["login_role_ID"] = $row["login_role_ID"];

        if (($db_password == $input_password) && ($db_status == "1"))
        {
          
          $sql_get_userinfo = "SELECT * FROM tb_userinfo WHERE userinfo_ID = '$db_userinfo_ID';";
          $result_get_userinfo = mysqli_query($con, $sql_get_userinfo);
          if ($result_get_userinfo->num_rows > 0)
          {
            if ($row = $result_get_userinfo->fetch_assoc())
            {
              $_SESSION["userinfo_ID"] = $row["userinfo_ID"];
              $_SESSION["userinfo_name"] = $row["userinfo_name"];
              $_SESSION["userinfo_lastname"] = $row["userinfo_lastname"];
              $_SESSION["userinfo_business_name"] = $row["userinfo_business_name"];
              $_SESSION["userinfo_gender"] = $row["userinfo_gender"];
              $_SESSION["userinfo_birthdate"] = $row["userinfo_birthdate"];
              $_SESSION["userinfo_nationality"] = $row["userinfo_nationality"];
              $_SESSION["userinfo_tax_ID"] = $row["userinfo_tax_ID"];
              $_SESSION["userinfo_register_date"] = $row["userinfo_register_date"];
              jsRedirect("replace", "home.php");
              exit(0);
            }
          }
          else
          {
            alert("ไม่พบข้อมูลผู้ใช้งาน");
            jsRedirect("replace", "login.php");
            exit(0);
          }
        }
        else 
        {
          alert("Username หรือ Password ไม่ถูกต้อง (0)");
          jsBack();
          exit(0);
        }
      }
    }
    else 
    {
      alert("Username หรือ Password ไม่ถูกต้อง (1)");
      jsBack();
      exit(0);
    }
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?=$GLOBALS["PROJECT_NAME"]?></title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <link rel="icon" href="../img/favicon.ico" type="image/gif" sizes="32x32">

</head>

<body class="bg-gradient-danger">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-3">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block "></div>
              <div class="col-lg-12">
                <img src="../assets/img/favicon.png" class="mx-auto d-block" style="width:30%"> 
              </div>
              <br>
              <div class="mx-auto" style="width: 400px;">
                  <div class="col align-self-center">
                    <div class="text-center">
                      <br>
                    <h1 class="h4 text-gray-900 mb-4">ยินดีต้อนรับ</h1>
                  </div>
                  <form class="user" action = "check_login.php" method = "POST">
                    <div class="form-group">
                        <input  type="text" class="form-control form-control-user" 
                                name = "tel" id = "tel"
                                placeholder = "ใส่ Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control form-control-user" 
                                name = "password1" id = "password1"
                                placeholder = "ใส่ Password">
                    </div>
                    <input type = "submit" class="btn btn-danger btn-user btn-block" value = "เข้าสู่ระบบ">
                  </form>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

 <!-- Bootstrap core JavaScript-->
 <script src="vendor/jquery/jquery.min.js"></script>
 <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <!-- Core plugin JavaScript-->
 <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

 <!-- Custom scripts for all pages-->
 <script src="js/sb-admin-2.min.js"></script>

 <!-- Page level plugins -->
 <script src="vendor/datatables/jquery.dataTables.min.js"></script>
 <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

 <!-- Page level custom scripts -->
 <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
