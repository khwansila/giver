<?php
    include '../connect.php';
    include '../main_function.php';
    session_start ();
    if (!empty($_SESSION[""]))
        echo "<script>window.location.replace('index.php')</script>";
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $username = $_POST["tel"];
        $password = md5($_POST["password1"]);

        $sql_get_login = "  SELECT tb_login.*, tb_user_role.role_ID 
                            FROM tb_login 
                            INNER JOIN tb_userinfo ON tb_login.ID = tb_userinfo.login_ID
                            LEFT JOIN tb_user_role ON tb_userinfo.ID = tb_user_role.userinfo_ID
                            WHERE username = '$username' LIMIT 1 ;";
        $result_get_login = mysqli_query($GLOBALS["con"], $sql_get_login);
        if ($result_get_login->num_rows > 0)
        {
            if ($row = $result_get_login->fetch_assoc())
            {
                $db_password = $row["password"];
                $userinfo_ID = $row["ID"];
                $userinfo_status = $row["status"];
                $role_ID = $row["role_ID"];
            }
        }
        if ($db_password != $password)
            alertBack("ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
        else if ($role_ID != 1)
            alertBack("เข้าได้เฉพาะ Admin เท่านั้น");
        else
        {
            //alert("userinfo = $userinfo_ID");
            $_SESSION["reset_password_username"] = "";
            $_SESSION["userinfo_ID"] = $userinfo_ID;
            $_SESSION["role_ID"] = $role_ID;
            jsRedirect("replace", "index.php");
        }
    }
?>