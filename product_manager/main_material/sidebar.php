<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
      
    <li>
      <div class="row my-2">
        <div class = "col text-center">
          <a><img class = "rounded-lg bg-white p-3" src="../assets/img/favicon.png" class="img-fluid flex-center" width = "60%"; height = "auto"></a>
        </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href="index.php">
        <i class="fas fa-home"></i>
        <span>Home</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="add_product.php">
        <i class="fas fa-home"></i>
        <span>เพิ่มสินค้า</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="add_notification.php">
        <i class="fas fa-home"></i>
        <span>สร้าง Notification</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="add_voucher.php">
        <i class="fas fa-home"></i>
        <span>เพิ่ม Voucher</span></a>
    </li>
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>