
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <div class = "row w-100">
              <div class = "col-10 text-center weight-font-bold text-dark"> 
                <div class = "mt-2">ระบบจัดการหลังบ้าน GIVER</div>
              </div>
              <div class = "col-2 text-right"> 
                  <input type = "button" class = "btn btn-danger mx-1" value = "ออกจากระบบ" onclick = "window.location.replace('logout.php');">
              </div>
          </div>

        </nav>