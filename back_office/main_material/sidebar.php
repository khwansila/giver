<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
      
    <li>
      <div class="row my-2">
        <div class = "col text-center">
          <a><img class = "rounded-lg bg-white p-3" src="../assets/img/favicon.png" class="img-fluid flex-center" width = "60%"; height = "auto"></a>
        </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href="index.php">
        <i class="fas fa-home"></i>
        <span>Home</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="add_product.php">
        <i class="fas fa-box"></i>
        <span>เพิ่มสินค้า</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="upload_product_pic.php">
        <i class="fas fa-upload"></i>
        <span>อัปโหลดรูปภาพสินค้า</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="store.php">
        <i class="fas fa-store-alt"></i>
        <span>รายชื่อร้านค้า</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="add_store.php">
        <i class="fas fa-store"></i>
        <span>เพิ่มร้านค้า</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="add_notification.php">
        <i class="fas fa-bell"></i>
        <span>สร้าง Notificaion</span></a>
    </li>

    <!-- <li class="nav-item">
      <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseNoti" aria-expanded="true" aria-controls="collapseNoti">
        <i class="fas fa-bell"></i>
        <span>จัดการ Notification</span>
      </a>
      <div id="collapseNoti" class="collapse" aria-labelledby="headingNoti" data-parent="#accordionSidebar" style="">
          <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">จัดการ Notification:</h6>
              <a class="collapse-item" href="add_notification.php">สร้าง Notification</a>
              <a class="collapse-item" href="delete_notification.php">ลบ Notification</a>
          </div>
      </div>
    </li> -->
    <li class="nav-item">
      <a class="nav-link" href="add_voucher.php">
        <i class="fas fa-ticket-alt"></i>
        <span>เพิ่ม Voucher</span></a>
    </li>
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>