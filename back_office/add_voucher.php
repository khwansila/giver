<?php
  include '../connect.php';
  include '../main_function.php';
  session_start();
  error_reporting(E_ERROR | E_PARSE);
  if (empty($_SESSION["userinfo_ID"]))
  {
    jsRedirect("replace", "login.php");
    exit(0);
  }
  if ($_SESSION["role_ID"] != 1)
  {
    session_destroy();
    alertGoto("หน้านี้เข้าได้เฉพาะ Admin เท่านั้น", "login.php");
    exit(0);
  }
  $store_option = get_store_option_from_arr (get_all_store ());
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'main_material/header.php'; ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <!-- Sidebar -->
  <?php include 'main_material/sidebar.php'; ?>
  <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include 'main_material/topbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <form action = "../frontend_operation.php" method = "POST" name = "add_voucher_form" id = "add_voucher_form">
                <div style = "display: none;">
                    <input type = "text" name = "operation_command" id = "operation_command" value = "add_voucher">
                </div>
                <div class="card shadow mb-4">
                    <div class="card-header">
                        เพิ่ม Voucher
                    </div>
                    <div class="card-body">
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                Barcode:
                            </div>
                            <div class = "col-9 text-right">
                                <input  type = "text" class = "form-control" 
                                        name = "barcode" id = "barcode"
                                        value = "" placeholder = "Barcode">
                            </div>
                        </div>
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                ชื่อ Voucher :
                            </div>
                            <div class = "col-9 text-right">
                                <input  type = "text" class = "form-control" 
                                        name = "name" id = "name"
                                        value = "" placeholder = "ชื่อ Voucher">
                            </div>
                        </div>
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                รายละเอียด Voucher :
                            </div>
                            <div class = "col-9 text-right">
                                <input  type = "text" class = "form-control" 
                                        name = "description" id = "description"
                                        value = "" placeholder = "รายละเอียด Voucher">
                            </div>
                        </div>
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                คุณสมบัติของ Voucher :
                            </div>
                            <div class = "col-9 text-right">
                                <input  type = "text" class = "form-control" 
                                        name = "spec" id = "spec"
                                        value = "" placeholder = "คุณสมบัติของ Voucher">
                            </div>
                        </div>
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                ราคา Voucher (บาท):
                            </div>
                            <div class = "col-9 text-right">
                                <input  type = "number" step = "0.25" class = "form-control" 
                                        name = "price" id = "price"
                                        value = "" placeholder = "ราคา Voucher (บาท)">
                            </div>
                        </div>
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                จำนวน Voucher :
                            </div>
                            <div class = "col-9 text-right">
                                <input  type = "text" class = "form-control" 
                                        name = "stock" id = "stock"
                                        value = "" placeholder = "จำนวน Voucher">
                            </div>
                        </div>
                        <div class = "row m-1">
                            <div class = "col-3 text-right">
                                ร้านค้าที่ใช้ :
                            </div>
                            <div class = "col-9 text-right">
                                <select class = "form-control"
                                        name = "store_ID" id = "store_ID">
                                    <?=$store_option?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class = "row my-1">
                            <div class = "col-1 ml-auto pr-1 text-right">
                                <input  type = "button" class = "btn btn-secondary" 
                                        value = "กลับ" onclick = "window.location.replace('index.php')">
                            </div>`
                            <div class = "col-1 pl-1 text-left">
                                <input  type = "button" class = "btn btn-danger" 
                                        value = "บันทึก" onclick = "document.getElementById('add_voucher_form').submit()">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php include 'main_material/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include 'main_material/modal.php'; ?>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
