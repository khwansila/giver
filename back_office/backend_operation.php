<?php
    include '../connect.php';
    include '../main_function.php';
    session_start();
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $operation_command = $_POST["operation_command"];

        if ($operation_command == "upload_product_pic")
        {
            $alert_msg = "";
            foreach($_FILES['fileToUpload']['tmp_name'] as $key => $val)
            {
                $exist_file = 1;
                while ($exist_file)
                {
                    $new_name = uniqid().uniqid();
                    $exist_file = check_exist_value ("name_onserver", "tb_product_pic", "name_onserver", $new_name);
                }

                $file_name = $_FILES['fileToUpload']['name'][$key];
                $file_size =$_FILES['fileToUpload']['size'][$key];
                $file_tmp =$_FILES['fileToUpload']['tmp_name'][$key];
                $file_type=$_FILES['fileToUpload']['type'][$key];  
                $pic_type = $_POST["pic_type"];

                if (($file_type != "image/jpeg") && ($file_type != "image/png"))
                    $alert_msg = "ไฟล์ $file_name.$file_type ไม่สามารถอัปโหลดได้เนื่องจากไม่ใช่ไฟล์รูปภาพ\n";
                else
                {
                    if ($file_type == "image/jpeg")
                        $extension = ".jpg";
                    else if ($file_type == "image/png")
                        $extension = ".png";
                    $target_dir = "../product_pic/";
                    move_uploaded_file($file_tmp,$target_dir.$new_name.$extension);

                    //image-size: h: 420 w: 860
                    $images = $target_dir.$new_name.$extension;
                    $new_images = $target_dir.$new_name.$extension;
                    if (($pic_type == "1") || ($pic_type == "2"))
                    {
                        $width=860; // Fix Width & Heigh (Autu caculate) 
                        $height=420;
                    }
                    else if ($pic_type == "3")
                    {
                        $width = 800;
                        $height = 800;
                    }
                    else
                    {
                        $height=round($width*$size[1]/$size[0]);
                    }
                    $size=GetimageSize($images);
                    //$height=round($width*$size[1]/$size[0]);
                    if ($extension == ".jpg")
                        $images_orig = ImageCreateFromJPEG($images);
                    else if ($extension == ".png")
                        $images_orig = ImageCreateFromPNG($images);
                    $photoX = ImagesX($images_orig);
                    $photoY = ImagesY($images_orig);
                    $images_fin = ImageCreateTrueColor($width, $height);
                    ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                    ImageJPEG($images_fin,$new_images);
                    ImageDestroy($images_orig);
                    ImageDestroy($images_fin);
                    $target_dir = "product_pic/";
                    insert_new_product_pic_info ($new_name, $file_name, $target_dir.$new_name.$extension);
                }
            }
            if ($alert_msg == "")
                alertGoto("อัปโหลดรูปภาพเรียบร้อยแล้ว", "upload_product_pic.php");
            else
                alertGoto($alert_msg, "upload_product_pic.php");
        }
        else if ($operation_command == "add_notification")
        {
            $owner_ID = $_SESSION["userinfo_ID"];
            
            $msg = $_POST["msg"];
            $action = $_POST["action"];
           
            $userinfo_list = get_userinfo_list ();
            $userinfo_arr = explode(",", $userinfo_list); 

            $i = 0;
            while ($userinfo_arr[$i] != "")
            {
                insert_new_notificaiton ($msg, $action, $userinfo_arr[$i]);
                $i++;
            }
            jsRedirect("replace", "add_notification.php");
        }
        else if ($operation_command == "add_product")
        {
            $barcode = $_POST["barcode"];
            $name = $_POST["name"];
            $description = $_POST["description"];
            $spec = $_POST["spec"];
            $price = $_POST["price"];
            $stock = $_POST["stock"];
            $share_commission = $_POST["share_commission"];
            $cashback = $_POST["cashback"];
            $bpoint = $_POST["bpoint"];
            $gpoint = $_POST["gpoint"];
            $result = add_product(sql_val($barcode), sql_val($name), sql_val($description), sql_val($spec), sql_val($price), sql_val($stock));
            if (!($result))
                alertBack("ไม่สามารถบันทึกสินค้าได้ กรุณาลองใหม่อีกครั้ง");
            else
                alert("บันทึกสินค้าเรียบร้อยแล้ว");
            jsRedirect("replace", "back_office/add_product.php");
        }
        else if ($operation_command == "add_voucher")
        {
            $barcode = $_POST["barcode"];
            $name = $_POST["name"];
            $description = $_POST["description"];
            $spec = $_POST["spec"];
            $price = $_POST["price"];
            $stock = $_POST["stock"];
            $store_ID = $_OST["store_ID"];
            $result = add_voucher(sql_val($barcode), sql_val($name), sql_val($description), sql_val($spec), sql_val($price), sql_val($stock), sql_val($store_ID));
            if (!($result))
                alertBack("ไม่สามารถบันทึกสินค้าได้ กรุณาลองใหม่อีกครั้ง");
            else
                alert("บันทึกสินค้าเรียบร้อยแล้ว");
            jsRedirect("replace", "product_manager/add_voucher.php");
        }
        else if ($operation_command == "add_store")
        {
            $store_name = $_POST["store_name"];
            $result = add_store($store_name);
            if (!($result))
                alertBack("ไม่สามารถเพิ่มร้านค้าได้ กรุณาลองใหม่อีกครั้ง");
            else
                alert("เพิ่มร้านค้าเรียบร้อยแล้ว");
            jsRedirect("replace", "add_store.php");
        }
    }
?>