<?php
  include '../connect.php';
  include '../main_function.php';
  session_start();
  error_reporting(E_ERROR | E_PARSE);
  if (empty($_SESSION["userinfo_ID"]))
  {
    jsRedirect("replace", "login.php");
    exit(0);
  }
  if ($_SESSION["role_ID"] != 1)
  {
    session_destroy();
    alertGoto("หน้านี้เข้าได้เฉพาะ Admin เท่านั้น", "login.php");
    exit(0);
  }
  $product_arr = get_all_products();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'main_material/header.php'; ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <!-- Sidebar -->
  <?php include 'main_material/sidebar.php'; ?>
  <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include 'main_material/topbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <form action = "backend_operation.php" method = "POST" name = "product_pic_form" id = "product_pic_form"  enctype="multipart/form-data">
                <div style = "display: none;">
                    <input type = "text"  name = "operation_command" id = "operation_command" value = "upload_product_pic">
                </div>
                <div class="card shadow mb-4">
                    <div class = "card-header">
                        <h5 class = "text-dark my-1">อัปโหลดรูปภาพสินค้า</h5>
                    </div>
                    <div class="card-body">
                        <div class = "row">
                            <div class = "col-2">
                                เลือกไฟล์รูปภาพที่ต้องการอัปโหลด
                            </div>
                            <div class = "col-10">
                                <input class = "form-control" style = "width: 100%;" type = "file" name = "fileToUpload[]" id = "fileToUpload" multiple="multiple" accept=".jpg, .jpeg, .png, .bmp">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <input type = "button" class = "btn btn-primary ml-auto" value = "อัปโหลดรูปภาพ" onclick = "document.getElementById('product_pic_form').submit();">
                    </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php include 'main_material/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include 'main_material/modal.php'; ?>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
