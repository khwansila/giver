<?php
  include '../connect.php';
  include '../main_function.php';
  session_start();
  error_reporting(E_ERROR | E_PARSE);
  if (empty($_SESSION["userinfo_ID"]))
  {
    jsRedirect("replace", "login.php");
    exit(0);
  }
  if ($_SESSION["role_ID"] != 1)
  {
    session_destroy();
    alertGoto("หน้านี้เข้าได้เฉพาะ Admin เท่านั้น", "login.php");
    exit(0);
  }
  $product_arr = get_all_products();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'main_material/header.php'; ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <!-- Sidebar -->
  <?php include 'main_material/sidebar.php'; ?>
  <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include 'main_material/topbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th class = "text-center">ที่</th>
                      <th class = "text-center">ชื่อสินค้า</th>
                      <th class = "text-center">รายละเอียดสินค้า</th>
                      <th class = "text-center">จำนวน</th>
                      <!-- <th class = "text-center">รายได้รวม (บาท)</th> -->
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    $i = 0;
                    $j = 1;
                    while ($product_arr[$i]["ID"] != "")
                    {
                  ?>
                    <tr>
                      <td class = "text-center"><?=$j?></td>
                      <td class = "text-center"><?=$product_arr[$i]["name"]?></td>
                      <td class = "text-center"><?=$product_arr[$i]["description"]?></td>
                      <td class = "text-center"><?=$product_arr[$i]["stock"]?></td>
                    </tr>
                  <?php
                      $i++; 
                      $j++;
                    } 
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php include 'main_material/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include 'main_material/modal.php'; ?>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

   <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
