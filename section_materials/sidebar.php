<div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">

                    <!-- profile box -->
                    <div class="profileBox">
                        <div class="image-wrapper">
                            <img src="img/materials/GIVER-LOGO-PNG.png" alt="image" class="imaged rounded bg-white">
                        </div>
                        <div class="in">
                            <strong><!-- Julian Gruber --></strong>
                            <div class="font-weight-bold">
                                <?php $my_info = get_userinfo ($_SESSION["userinfo_ID"]); ?>
                                <span style = "color: white;"><?=$my_info[0]["username"].":".$my_info[0]["name"]." ".$my_info[0]["lastname"]?></span>
                                <!-- <ion-icon name="location"></ion-icon> -->
                                <!-- California -->
                            </div>
                        </div>
                        <a href="javascript:;" class="close-sidebar-button" data-dismiss="modal">
                            <ion-icon name="close"></ion-icon>
                        </a>
                    </div>
                    <!-- * profile box -->

                    <ul class="listview flush transparent no-line image-listview mt-2">
                        <li>
                            <a href="notification.php" class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="notifications-circle-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Notification</div>
                                    <!-- <span class="badge badge-danger">5</span> -->
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="chat.php" class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Chat</div>
                                    <!-- <span class="badge badge-danger">5</span> -->
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="profile.php" class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="person-circle-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Profile ของฉัน</div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="setting.php" class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="settings-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>ตั้งค่า</div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="voucher_available.php" class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="checkmark-circle-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Voucher ที่มีอยู่</div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="voucher_used.php" class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="checkmark-done-circle-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Voucher ที่ใช้แล้ว</div>
                                </div>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#" class="item" onclick = "localStorage.clear();">
                                <div class="icon-box bg-danger">
                                    <ion-icon name="close-circle-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Clear cache</div>
                                    <span class="badge badge-danger">5</span>
                                </div>
                            </a>
                        </li> -->
                        <li>
                            <div class="item">
                                <div class="icon-box bg-danger"> <!-- -->
                                    <ion-icon name="moon-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Dark Mode</div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input dark-mode-switch"
                                            id="darkmodesidebar">
                                        <label class="custom-control-label" for="darkmodesidebar"></label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="sidebar-buttons">
                    <input type = "button" class = "btn btn-danger btn-block mx-3 mb-1" value = "ออกจากระบบ" onclick = "window.location.replace('logout.php')">
                </div>
                <!-- sidebar buttons -->
                <!-- 
                <div class="sidebar-buttons">
                    <a href="javascript:;" class="button">
                        <ion-icon name="person-outline"></ion-icon>
                    </a>
                    <a href="javascript:;" class="button">
                        <ion-icon name="archive-outline"></ion-icon>
                    </a>
                    <a href="javascript:;" class="button">
                        <ion-icon name="settings-outline"></ion-icon>
                    </a>
                    <a href="javascript:;" class="button">
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </div>
                 -->
                <!-- * sidebar buttons -->
            </div>
        </div>
    </div>