<!-- App Bottom Menu -->
<div class="appBottomMenu" style = "background-color: red;">
        <a href="index.php" class="item">
            <div class="col">
                <ion-icon name="home-outline" style = "color: white"></ion-icon>
            </div>
        </a>
        <a href="reward.php" class="item">
            <div class="col rounded bg-white py-2">
                <ion-icon name="heart-outline" style = "color: red"></ion-icon>
            </div>
        </a>
        <a href="giver.php" class="item ">
            <div class="col">
                <!-- <ion-icon name="cube-outline"></ion-icon>
                <span class="badge badge-danger">5</span> -->
                <img src="img/materials/logo_giver_bar.png" width = "50%">
            </div>
        </a>
        <a href="page-cart.php" class="item">
            <div class="col">
                <ion-icon name="cart-outline" style = "color: white"></ion-icon>
            </div>
        </a>
        <a href="javascript:;" class="item" data-toggle="modal" data-target="#sidebarPanel">
            <div class="col">
                <ion-icon name="person-outline" style = "color: white"></ion-icon>
            </div>
        </a>
    </div>
    <!-- * App Bottom Menu -->