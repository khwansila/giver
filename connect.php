<?php
    function alert($msg)
    {
        echo "<script>alert('$msg');</script>";
    }
    $GLOBALS["PROJECT_NAME"] = "GIVER";
    $GLOBALS["DOMAIN_NAME"] = "GIVER";
    $GLOBALS["PROJECT_KEYWORD"] = "giver, shopping, online shopping, shopping application, ช็อปปิ้งออนไลน์, รายได้";
    $GLOBALS["PROJECT_DESCRIPTION"] = "Giver";
    $GLOBALS["PROJECT_AUTHOR"] = "";
/*     
    $GLOBALS["PROJECT_THEME_COLOR"] = "danger ";
    $GLOBALS["PROJECT_COLOR_BUTTON"] = "warning ";
    $GLOBALS["PROJECT_COLOR_CODE"] = "#ff0000";  
*/
    // warning-#ffc107 primary-#007bff danger-#dc354 info-#17a2b8 red-#ff0000
    $environment = "1"; // 0-> localhost 1->server

    if (($_SERVER["HTTP_HOST"] == "localhost") || ((strpos($_SERVER["HTTP_HOST"], "192.168.") !== false)))
        $environment = "0"; // 0-> localhost 1->server
    else
        $environment = "1"; // 0-> localhost 1->server
    if ($environment == "0")
    {
        $GLOBALS["con"]= mysqli_connect("localhost:3306","root","","giver") or die("Error: " . mysqli_error($con));
        $GLOBALS["HOSTNAME"] = "http://localhost/giver";
        mysqli_query($con, "SET NAMES 'utf8' ");
        error_reporting( error_reporting() & ~E_NOTICE );
        date_default_timezone_set('Asia/Bangkok');
    }

    else if ($environment == "1")
    {
        $GLOBALS["con"]= mysqli_connect("localhost:3306","admin_giverapps","sppapas-revig","admin_giverapps") or die("Error: " . mysqli_error($con));
        $GLOBALS["HOSTNAME"] = "https://www.giverapps.com";
        mysqli_query($con, "SET NAMES 'utf8' ");
        error_reporting( error_reporting() & ~E_NOTICE );
        date_default_timezone_set('Asia/Bangkok');
    }
?>