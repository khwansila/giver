<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    $page_header = "Business Giver";
    //alert(($_SESSION["userinfo_ID"]));
    $userinfo = get_userinfo($_SESSION["userinfo_ID"]);
    $product_arr = get_all_products();
    $sum_cash = number_format(get_my_sum_cash ($_SESSION["userinfo_ID"]));
    if (get_giver_business_status($_SESSION["userinfo_ID"]) == 1)
    {
        $btn_value = "แลกซื้อ";
        $btn_url = "";
    }
        
    else
    {
        $btn_value = "สมัครสมาชิก";
        $btn_url = "";
    }
    $bpoint = get_bpoint($_SESSION["userinfo_ID"]);
    $gpoint = get_gpoint($_SESSION["userinfo_ID"]);
        
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

    <style>
        ion-icon {
        color: white;
        }
        * {box-sizing:border-box}

        /* Slideshow container */
        .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
        }

        /* Hide the images by default */
        .mySlides {
        display: none;
        }

        /* slide_slide_text & slide_previous buttons */
        .slide_prev, .slide_slide_text {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        }

        /* Position the "slide_slide_text button" to the right */
        .slide_slide_text {
        right: 0;
        border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .slide_prev:hover, .slide_slide_text:hover {
        background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
        }

        .slide_active, .dot:hover {
        background-color: #717171;
        }

        /* Fading animation */
        .slide_fade {
        -webkit-animation-name: slide_fade;
        -webkit-animation-duration: 1.5s;
        animation-name: slide_fade;
        animation-duration: 1.5s;
        }

        @-webkit-keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }

        @keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }
    </style>
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- Header -->
    <?php include 'section_materials/topbar.php';?>
    <!-- Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div class="section mt-3 mb-3">
            <div class="card text-white mb-2" style = "background-color: #17a2b8;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body pb-1">
                    <div class="row w-100">
                        <div class="col-4 text-center bg-white py-2" style = "border-radius: 10%;">
                            <img src = "img/materials/logogiver2.png" width= "50%">
                        </div>
                        <div class="col-8">
                            <div class = "row mt-1">
                                <div class = "col-sm-12 col-md-4 col-xl-4 text-left my-1">ชื่อ: <?=$userinfo[0]["name"]?></div>
                            </div>
                            <div class = "row">
                                <div class = "col-sm-12 col-md-4 col-xl-4 text-left my-1">รหัสคำเชิญ: <?=$userinfo[0]["username"]?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100">
                        <input type = "text" style = "display: none;" id = "invite_code" value = "<?=$userinfo[0]["username"]?>">
                        <div class = "col-sm-12 col-md-4 col-xl-4 text-left my-1 text-center">
                            <input type = "button" class = "btn btn-danger rounded" value = "คัดลอกรหัสคำเชิญ" onclick = "notification('copy_noti', 3000);Clipboard_CopyTo('<?=$userinfo[0]["username"]?>');">
                            <input type = "button" class = "btn btn-danger rounded" data-toggle="modal" data-target="#ModalListview"  value = "แชร์คำเชิญ">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body py-2">
                    <div class="row w-100">
                        <div class="col-8 my-auto font-weight-bold text-dark">
                            ข้อเสนอเพิ่มเติม
                        </div>
                        <div class="col-4">
                           <input type = "button" class = "btn btn-danger rounded" value = "อัปเกรดตอนนี้" onclick = "window.location.replace('offer.php')">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center text-dark py-2">
                    <div class="row w-100">
                        <div class="col-6 my-auto font-weight-bold text-dark text-center">
                            รายได้ทั้งหมด
                        </div>
                        <div class="col-6 my-auto font-weight-bold text-danger text-center">
                            <span style = "font-size: 20px;"><?=$sum_cash?> บาท</span>
                        </div>
                    </div>
                    <div class="row w-100">
                        <div class="col-12 my-auto font-weight-bold text-dark text-center">
                            <input type = "button" class = "btn btn-danger rounded my-1" value = "ถอนเงิน" onclick = "window.location.href='add_bank.php';">
                        </div>
                        <div class="col-12 my-auto font-weight-bold text-dark text-center">
                            <span style = "font-size: 14px;">ถอนได้ขั้นต่ำ 1,000 บาท</span>
                        </div>
                    </div>
                    <hr>
                    <div class="row w-100 mt-1">
                        <div class="col-6 my-auto border border-secondary  border-top-0 border-left-0 border-bottom-0">
                            <div class = "text-center font-weight-bold text-danger my-1">
                                <span style = "font-size: 20px;">0 บาท</span>
                            </div>
                            <div class = "text-center font-weight-bold">
                                รายได้วันนี้
                            </div>
                        </div>
                        <div class="col-6 my-auto">
                            <div class = "text-center font-weight-bold text-danger my-1">
                            <div calss = "row"><div class = "col-12 mx-0 px-0"><span class = "my-1" style = "font-size: 20px;"> 0 บาท</span></div></div>
                                <!-- <div calss = "row"><div class = "col-12 mx-0 px-0"><span class = "my-1" style = "font-size: 16px;">ชั้นที่ 1 (35%) : 0 บาท</span></div></div>
                                <div calss = "row"><div class = "col-12 mx-0 px-0"><span class = "my-1" style = "font-size: 16px;">ชั้นที่ 2 (40%) : 0 บาท</span></div></div>
                                <div calss = "row"><div class = "col-12 mx-0 px-0"><span class = "my-1" style = "font-size: 16px;">ชั้นที่ 3 (50%) : 0 บาท</span></div></div> -->
                            </div>
                            <div class = "text-center font-weight-bold">
                                ค่าคอมมิชชั่นวันนี้
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <div class="col-6">
                            <div class = "my-auto font-weight-bold text-dark text-center">คะแนนสะสม GP</div>
                            <div class = "my-auto font-weight-bold text-danger text-center">
                                <span style = "font-size: 20px;"><?=number_format($gpoint)?> คะแนน</span>
                            </div>
                        </div>
                        <div class="col-6 my-auto font-weight-bold text-danger text-center">
                            <div class = "">
                                <input type = "button" class = "btn btn-danger rounded" value = "แลกซื้อ" onclick = "window.location.href='reward_g_exchange.php';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <div class="col-6">
                            <div class = "my-auto font-weight-bold text-dark text-center">คะแนนสะสม BP</div>
                            <div class = "my-auto font-weight-bold text-danger text-center">
                                <span style = "font-size: 20px;"><?=number_format($bpoint)?> บาท</span>
                            </div>
                        </div>
                        <div class="col-6 my-auto font-weight-bold text-danger text-center">
                            <div class = "">
                                <input type = "button" class = "btn btn-danger rounded" id = "bpoint_btn" value = "<?=$btn_value?>" onclick = "bpoint_btn()">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class = "font-weight-bold text-dark">สินค้าสำหรับสมาชิก Giver</div>
            <hr>
            <div class="card mb-2">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <?php
                            $i = 0;
                            while ($product_arr[$i]["ID"] != "")
                            {
                        ?>
                        <div class="col-6 my-1">
                            <div class="card product-card">
                                <div class="card-body">
                                    <img src="product_pic/<?=$product_arr[$i]["pic_url"]?>" class="image" alt="product image" height = "30%">
                                    <h2 class="title"><?=$product_arr[$i]["name"]?></h2>
                                    <div class="price"><?=$product_arr[$i]["price"]?> บาท</div>
                                    <div class=""><?=$product_arr[$i]["price"]//$product_arr[$i]["bpoint"]?> Bpoint</div>
                                    <div class="mb-1">1 <!-- สินค้า (SKU) : <br> --><?//=$product_arr[$i]["gpoint"]?> Gpoint</div>
                                    <a href="#" class="btn btn-sm btn-primary btn-block" onclick = "add_cart ('<?=$product_arr[$i]["ID"]?>')">ใส่ตะกร้า</a>
                                </div>
                            </div>
                        </div>
                        <?php
                                $i++;
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-3" id = "help">
            <div class="card mb-2"  style = "background-color: #17a2b8;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <div class = "col-4 rounded text-white text-center" onclick = "window.location.href = 'order.php'">
                            <ion-icon name="document-text-outline" style = "font-size: 64px;"></ion-icon>
                            ประวัติการซื้อ
                        </div>
                        <div class = "col-4 rounded text-white text-center">
                            <ion-icon name="school-outline" style = "font-size: 64px;"></ion-icon>
                            ศูนย์การเรียนรู้
                        </div>
                        <div class = "col-4 rounded text-white text-center">
                            <ion-icon name="call-outline" style = "font-size: 64px;"></ion-icon>
                            ติดต่อบริษัท
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-1">
            <div class = "font-weight-bold text-dark">ตำแหน่งธุรกิจ</div>
            <hr>
            <div class="card mb-2"  style = "background-color: #ffffff;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center ">
                    <div class="row text-center">
                        <!-- <div class = "col-5 py-3 pt-4 ml-auto mr-1 rounded text-white text-center" style = "font-size: 32px; background-color: red;">
                            MD
                        </div> -->
                        <div class = "col-11 py-3 mr-auto ml-1 rounded text-white text-center" style = "background-color: #17a2b8;">
                            <div style = "font-size: 24px;">10</div>
                            <div>ทีมงานของฉัน</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
        <?php include 'section_materials/bottom_menu_3.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- Modal Listview -->
    <div class="modal fade modalbox" id="ModalListview" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">แชร์</h5>
                    <a href="javascript:;" data-dismiss="modal">ปิด</a>
                </div>
                <div class="modal-body p-0">

                    <ul class="listview image-listview flush mb-2">
                        <li onclick = "share_fb()">
                            <div class="item">
                                <img src="img/logo/facebook_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Facebook</div>
                                </div>
                            </div>
                        </li>
                        <li onclick = "share_line()">
                            <div class="item">
                                <img src="img/logo/line_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Line</div>
                                </div>
                            </div>
                        </li><!-- 
                        <li onclick = "copyToClipboard()">
                            <div class="item">
                                <img src="img/logo/link_icon.png" alt="image" class="image">
                                <div class="in">
                                    <div>คัดลอกลิงก์</div>
                                </div>
                            </div>
                        </li> -->
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div id="copy_noti" class="notification-box">
        <div class="notification-dialog ios-style">
            <div class="notification-header">
                <div class="in">
                    <!-- <img src="assets/img/sample/avatar/avatar3.jpg" alt="image" class="imaged w24 rounded"> -->
                    <strong>การแจ้งเตือน</strong>
                </div>
                <div class="right">
                    <span>just now</span>
                    <a href="#" class="close-button">
                        <ion-icon name="close-circle"></ion-icon>
                    </a>
                </div>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">คัดลอกลิงก์เรียบร้อยแล้ว</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- welcome notification  -->
    <!-- <div id="notification-welcome" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="assets/img/icon/72x72.png" alt="image" class="imaged w24">
                    <strong>Mobilekit</strong>
                    <span>just now</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Welcome to Mobilekit</h3>
                    <div class="text">
                        Mobilekit is a PWA ready Mobile UI Kit Template.
                        Great way to start your mobile websites and pwa projects.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- * welcome notification -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    
    <script src="assets/js/main_function.js"></script>


    <script>
        setTimeout(() => {
            notification('notification-welcome', 5000);
        }, 2000);
    </script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        // slide_slide_text/slide_previous controls
        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" slide_active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " slide_active";
        }

        function show_load ()
        {
            async function main_func() 
            {
                // task 1
                document.getElementById('loader').style.display = '';
                
                // tast 2
                const result = await new Promise (resolve => {$("#loader").fadeToggle(500); resolve(1);});
            }
            main_func();
        }

        function Clipboard_CopyTo(value) {
        var tempInput = document.createElement("input");
        tempInput.value = 'https://www.giverapps.com/signup_selection.php?invite_code='+value;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
        //alert("คัดลอกรหัสคำเชิญเรียบร้อยแล้ว");
        }

        document.querySelector('#Copy').onclick = function() {
        Clipboard_CopyTo('Text to copy!');
        }

        function share_fb()
        {
           /*  window.open(
                        'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
        function share_line()
        {
           /*  window.open(
                        'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
        function getXMLHTTP() 
        {
            var x = false;
            try {
                x = new XMLHttpRequest();
            }catch(e) {
                try {
                    x = new ActiveXObject("Microsoft.XMLHTTP");
                    }catch(ex) {
                    try {
                        req = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch(e1) {
                        x = false;
                    }
                }
            }
            return x;
        }
        function bpoint_btn()
        {
            if (document.getElementById('bpoint_btn').value == 'สมัครสมาชิก')
            {
                var strURL = "ajax_request.php";
                var req = getXMLHTTP();
                if (req)
                {
                    req.onreadystatechange = function() 
                    {
                        if (req.readyState == 4) 
                        {
                            // only if "OK"
                            if (req.status == 200) 
                            {						
                                if (req.responseText == "1")
                                {
                                    alert("สมัครสมาชิกเรียบร้อยแล้ว");
                                    document.getElementById('bpoint_btn').value = 'แลกซื้อ';
                                    window.location.href = 'reward_b_exchange.php';
                                }
                            } 
                            else 
                            {
                                alert("Problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    }			
                    req.open("POST", strURL, true);
                    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    req.send("operation_command=regis_giver_business&userinfo_ID="+<?=$_SESSION["userinfo_ID"]?>);
                }	
            }
            else if (document.getElementById('bpoint_btn').value == 'แลกซื้อ')
            {
                window.location.href = 'reward_b_exchange.php';
            }
            	
        }
        async function add_cart (val)
        {
            var quantity = 1;//document.getElementById('quantity').value;
            var result = ajax_req("ajax_request.php", "operation_command=add_to_cart&owner_ID=<?=$_SESSION["userinfo_ID"]?>&product_ID="+val+"&quantity=1", after_ajax);
            function after_ajax(result)
            {
                if (result == "1")
                {
                    alert('ใส่สินค้าลงตะกร้าเรียบร้อยแล้ว');
                }
                else
                {
                    alert('ใส่สินค้าลงตะกร้าไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');
                }
                /* else if (result != "1")
                {
                    if ((password_1_val != "") && (password_2_val != "") && (password_1_val == password_2_val) && (tel_val != ""))
                    {
                        document.getElementById('username_exist').style.display = 'none';
                        document.getElementById('submit_div').style.display = '';
                    }
                } */
            }
        }
    </script>
</body>

</html>