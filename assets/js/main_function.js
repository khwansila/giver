function getXMLHTTP() 
{
    var x = false;
    try {
    x = new XMLHttpRequest();
    }catch(e) {
    try {
        x = new ActiveXObject("Microsoft.XMLHTTP");
    }catch(ex) {
        try {
        req = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch(e1) {
        x = false;
        }
    }
    }
    return x;
}

function ajax_req(url, parameters, getFucn) 
{		
    var strURL = url;
    var req = getXMLHTTP();
    var result;
    if (req)
    {
        req.onreadystatechange = function() 
        {
            if (req.readyState == 4) 
            {
                // only if "OK"
                if (req.status == 200) 
                {				
                    getFucn(req.responseText);
                } 
                else 
                {
                    //alert("Problem while using XMLHTTP: " + req.statusText);
                }
            }
        }			
        req.open("POST", strURL, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(parameters);
    }	
}
function seq_function ()
{
    function after_func() {
    return new Promise( resolve => {
                                        resolve("task 2");
                                    }
                        );
    }

    async function main_func() 
    {
        alert('task 1');
        const result = await after_func();
        alert(result);
    }

    main_func();
}

function seq_function_2 ()
{
    
    async function main_func() 
    {
        // task 1
        alert('task 1');

        // tast 2
        const result = await new Promise (resolve => {setTimeout( () => { resolve('task 2'); }, 2000);  });
        alert(result);
    }
    main_func();
}