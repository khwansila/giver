<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    //alert(($_SESSION["userinfo_ID"]));
    $cart = get_cart ($_SESSION["userinfo_ID"]);
    $product_ID_list = $cart[0]["product_ID_list"];
    $product_ID_arr = explode(",", $cart[0]["product_ID_list"]);
    $product_detail_arr = get_product_detail_from_list ($product_ID_list);
    $quantity_arr = explode(",", $cart[0]["quantity_list"]);
    $price_arr = explode(",", $cart[0]["price_list"]);
    $val = rand(1,99999999);
    $reward_arr = get_available_voucher_by_owner ($_SESSION["userinfo_ID"]);
    //print_r($reward_arr);
    //$reward_arr = get_all_reward();
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-danger text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Voucher ที่มีอยู่</div>
        <!-- <div class="right">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#DialogClear">
                <ion-icon name="trash-outline"></ion-icon>
            </a>
        </div> -->
    </div>
    <!-- * App Header -->

    <!-- Dialog Clear -->
    <div class="modal fade dialogbox" id="DialogClear" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Empty Cart</h5>
                </div>
                <div class="modal-body">
                    All items will be deleted.
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CLOSE</a>
                        <a href="#" class="btn btn-text-danger" data-dismiss="modal">DELETE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * Dialog Clear -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <form method="post" action="https://www.thaiepay.com/epaylink/payment.aspx">
            
            <div class="section mt-2">
                <?php
                $net_price = 0;
                $i = 0;
                $j = 1;
                while (($reward_arr[$i]["ID"] != ""))
                {
                    $uid = $reward_arr[$i]["uid"];
                    $uid = $reward_arr[$i]["serial"];
                    $description = $reward_arr[$i]["description"];
                ?>
                <!-- item -->
                <div class="card cart-item mb-2" id = "product_<?=$i?>">
                    <div class="card-body">
                        <div class="in">
                            <img src="product_pic/<?=$reward_arr[$i]["pic_url"]?>" alt="product" class="imaged">
                            <div class="text">
                                <h3 class="title"><?=$reward_arr[$i]["name"]?></h3>
                                <!-- <p class="detail"> <?//=$quantity_arr[$i]?> ชิ้น</p> -->
                                <strong class="price"><?=number_format($reward_arr[$i]["gpoint_exchange"])?> Gpoint/<?=number_format($reward_arr[$i]["bpoint_exchange"])?> Bpoint</strong>
                            </div>
                        </div>
                        <div class="cart-item-footer">
                            <a  href="#" onclick = "document.getElementById('qrcode').innerHTML = ''; generate_qrcode('<?=$uid?>', '<?=$serial?>'); document.getElementById('voucher_uid').value = '<?=$uid?>'; document.getElementById('voucher_description').value = '<?=$description?>';" 
                                class="btn btn-danger btn-sm ml-auto"
                                data-toggle="modal" data-target="#ModalBasic">
                                ใช้ Voucher นี้
                            </a>
                        </div>
                    </div>
                </div>
                <!-- * item -->
                <?php
                $net_price += $price_arr[$i];
                $i++;
                $j++;
                }
                ?>
            </div>
        </form>
    </div>
    <!-- * App Capsule -->

    <!-- App Bottom Menu -->
    <?php include 'section_materials/bottom_menu_5.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- Modal Basic -->
    <div class="modal fade modalbox" id="ModalBasic" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">QR Code Voucher</h5>
                    <a href="javascript:;" data-dismiss="modal">Close</a>
                </div>
                <div class="modal-body">
                    <div class = "row mx-1">
                        <div class = "col-4 my-1 text-right">
                            เลขที่ Voucher
                        </div>
                        <div class = "col-8 my-1 text-left">
                            <input  type = "text" class = "form-control" 
                                    name = "voucher_uid" id = "voucher_uid"
                                    value = "" placeholder = "ไอดีของ Voucher" readonly>
                        </div>
                    </div>
                    <div class = "row mx-1">
                        <div class = "col-4 my-1 text-right">
                            รายละเอียด Voucher
                        </div>
                        <div class = "col-8 my-1 text-left">
                            <textarea   type = "text" class = "form-control" 
                                        name = "voucher_description" id = "voucher_description"
                                        value = "" placeholder = "รายละเอียดของ Voucher" readonly></textarea>
                        </div>
                    </div>
                    <div class = "row mx-1">
                        <div class = "col-12 my-1 text-left">
                            QR Code
                        </div>
                    </div>
                    <div class = "row mx-1">
                        <div id = "qrcode" class = "col-12 my-1 text-center">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * Modal Basic -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

    <script src="assets/js/main_function.js"></script>
    <script src="assets/js/qrcode.min.js"></script>

    <script>
        async function delete_cart_item (position, product_position)
        {
            //var quantity = document.getElementById('quantity').value;
            result = ajax_req("ajax_request.php", "operation_command=delete_cart_item&owner_ID=<?=$_SESSION["userinfo_ID"]?>&position="+position, after_ajax);
            async function after_ajax(result, update_net_price)
            {
               if (result == "1")
                {
                    //alert('ใส่สินค้าลงตะกร้าเรียบร้อยแล้ว');
                    document.getElementById(product_position).style.display = "none";
                    ajax_req("ajax_request.php", "operation_command=update_net_price&owner_ID=<?=$_SESSION["userinfo_ID"]?>", update_net_price);
                }
                else
                {
                    //alert('ใส่สินค้าลงตะกร้าไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');
                }
                /* else if (result != "1")
                {
                    if ((password_1_val != "") && (password_2_val != "") && (password_1_val == password_2_val) && (tel_val != ""))
                    {
                        document.getElementById('username_exist').style.display = 'none';
                        document.getElementById('submit_div').style.display = '';
                    }
                } */ 
                function update_net_price (val)
                {
                    document.getElementById('net_price').innerHTML = val;
                    document.getElementById('total').value = val.replace(",","")
                }
            }
        }
        function generate_qrcode(uid, serial)
        {   
            new QRCode(document.getElementById("qrcode"), "http://www.giverapps.com/voucher_usage/index.php?voucher_uid="+uid+"&voucher_key="=serial);
        }
    </script>

</body>

</html>