<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (!empty($_SESSION["userinfo_ID"]))
        jsRedirect("replace", "index.php");

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $operation_command = $_POST["operation_command"] == "" ? "" : $_POST["operation_command"];
        if ($operation_command == "check_invite_code")
        {
            $suggest_ID = get_userinfo_ID_from_invite_code ($_POST["invite_code"]);
            $owner_ID = get_userinfo_ID_from_invite_code($_POST["owner_ID"]);
            $result = "";
            if (!empty($suggest_ID))
            {
                $result = insert_suggest ($owner_ID, $suggest_ID);
            }
            if ($result)
            {
                alertGoto("สมัครสมาชิกเรียบร้อย", "login.php");
            }
        }
    }
?>
