<?php
    session_start();
    if (basename(__FILE__) == basename($_SERVER["connect.php"])) 
    {
        //echo "called directly";
    } 
    else 
    {
        //echo "included/required";
        include 'connect.php';
    }
?>

<!-- App Capsule -->
<!-- <div id="appCapsule"> -->

<div class="header-large-title">
    <h1 class="title">Welcome to Giver</h1>
    <h4 class="subtitle" style = "display: none">Welcome to GIVER.</h4>
</div>

<div class="section full mt-3 mb-3">
    <div class="slideshow-container">

        <!-- Full-width images with number and caption text -->
        <div class="mySlides slide_fade">
                <div class="numbertext"><!-- 1 / 3 --></div>
                    <img src="img/materials/1-1200x656.jpg" style="width: 100%;">
                <div class="text"><!-- Caption Text --></div>
        </div>
    
        <div class="mySlides slide_fade">
            <div class="numbertext"><!-- 2 / 3 --></div>
            <img src="img/materials/2-1200x656.jpg" style="width: 100%;">
            <div class="text"><!-- Caption Two --></div>
        </div>
    
        <!-- <div class="mySlides slide_fade">
            <div class="numbertext">3 / 3</div>
            <img src="img3.jpg" style="width:100%">
            <div class="text">Caption Three</div>
        </div> -->
    
        <!-- slide_slide_text and slide_previous buttons -->
        <a class="slide_prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="slide_slide_text" onclick="plusSlides(1)">&#10095;</a>
    </div>
</div>
<div class="section full mt-3 mb-3">
    <div class="carousel-multiple owl-carousel owl-theme">
        <div class="item">
            <div class="card">
                <img src="https://th-live-01.slatic.net/p/5a0449a5d434d0218fce6c4989a585b2.jpg"  width = "320px" height = "auto" class="card-img-top" alt="image">
                <div class="card-body pt-2">
                    <h4 class="mb-0">สินค้าชิ้นที่ 1</h4>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <img src="https://th-live-01.slatic.net/p/181ff6e5913ca884ea7d2f82a7e74fe3.png"  width = "auto" height = "auto" class="card-img-top" alt="image">
                <div class="card-body pt-2">
                    <h4 class="mb-0">สินค้าชิ้นที่ 2</h4>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <img src="https://th-live-01.slatic.net/p/0808f8621f32f79a0173442b5dace5c7.jpg"  width = "320px" height = "auto" class="card-img-top" alt="image">
                <div class="card-body pt-2">
                    <h4 class="mb-0">สินค้าชิ้นที่ 3</h4>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <img src="https://th-live-01.slatic.net/p/593d00ff5244eb38efae89ade1a48ee4.jpg"  width = "320px" height = "auto" class="card-img-top" alt="image">
                <div class="card-body pt-2">
                    <h4 class="mb-0">สินค้าชิ้นที่ 4</h4>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <img src="https://th-live-01.slatic.net/p/3e74dc72101f596a947e29d1628b3143.jpg" width = "320px" height = "auto"  class="card-img-top" alt="image">
                <div class="card-body pt-2">
                    <h4 class="mb-0">สินค้าชิ้นที่ 5</h4>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <img src="https://th-live-01.slatic.net/p/0df1e769244caf60ba518009aae0495e.jpg"  width = "320px" height = "auto" class="card-img-top" alt="image">
                <div class="card-body pt-2">
                    <h4 class="mb-0">สินค้าชิ้นที่ 6</h4>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="section mt-3 mb-3">
    <div class="card">
        <!-- <img src="assets/img/sample/photo/wide4.jpg" class="card-img-top" alt="image"> -->
        <div class="card-body">
            <div class = "row text-center">
                <div class = "col-3" onclick = "show_load()">
                    <img src="img/materials/catagories/menu_1.png" width = "auto">
                </div>
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_2.png" width = "auto">
                </div>
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_3.png" width = "auto">
                </div>
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_4.png" width = "auto">
                </div>
            </div>
            <div class = "row text-center">
                <div class = "col-3">
                    <label>อาหารเสริมลดน้ำหนัก</label>
                </div>
                <div class = "col-3">
                    <label>อาหารเสริมเวย์โปรตีน</label>
                </div>
                <div class = "col-3">
                    <label>อาหารเสริมบำรุงผิว</label>
                </div>
                <div class = "col-3">
                    <label>เซรั่มบำรุงผิว</label>
                </div>
            </div>
            <div class = "row text-center">
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_5.png" width = "auto">
                </div>
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_6.png" width = "auto">
                </div>
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_7.png" width = "auto">
                </div>
                <div class = "col-3">
                    <img src="img/materials/catagories/menu_8.png" width = "auto">
                </div>
            </div>
            <div class = "row text-center">
                <div class = "col-3">
                    <label>ทำความสะอาดผิว</label>
                </div>
                <div class = "col-3">
                    <label>ครีมบำรุงผิว</label>
                </div>
                <div class = "col-3">
                    <label>ครีมกันแดด</label>
                </div>
                <div class = "col-3">
                    <label>เครื่องสำอาง</label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section mt-3 mb-3">
    <div class="card text-white bg-warning mb-2">
        <div class="card-header"><img src = "img/materials/flash_sale.png" witdh = "auto"></div>
        <div class="card-body">
            <h5 class="card-title">ลดสูงสุด 90% เฉพาะก่อนเที่ยงคืนเท่านั้น</h5>
                <div class="row">
                    <div class="col-6">
                        <div class="card product-card">
                            <div class="card-body">
                                <img src="https://my-live-02.slatic.net/p/2948bc11eb39a2632867ba0f86632f00.jpg" class="image" alt="product image">
                                <h2 class="title">สินค้าลดราคา 1</h2>
                                <div class="price">฿ 1.50</div>
                                <a href="#" class="btn btn-sm btn-primary btn-block">ADD TO CART</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card product-card">
                            <div class="card-body">
                                <img src="https://th-test-11.slatic.net/p/2/apple-lightning-to-usb-cable-2-m-1511778929-68753817-e653efd50030200de15c8c5a85b017d1-catalog.jpg_150x150q80.jpg" class="image" alt="product image">
                                <h2 class="title">สินค้าลดราคา 2</h2>
                                <div class="price">฿ 4.99</div>
                                <a href="#" class="btn btn-sm btn-primary btn-block">ADD TO CART</a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- <div class="section mt-3 mb-3">
    <div class="card">
        <div class="card-body d-flex justify-content-between align-items-end">
            <div>
                <h6 class="card-subtitle">Discover</h6>
                <h5 class="card-title mb-0 d-flex align-items-center justify-content-between">
                    Dark Mode
                </h5>
            </div>
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input dark-mode-switch" id="darkmodeswitch">
                <label class="custom-control-label" for="darkmodeswitch"></label>
            </div>

        </div>
    </div>
</div> -->

<!-- <div class="section mt-3 mb-3">
    <div class="card">
        <img src="assets/img/sample/photo/wide4.jpg" class="card-img-top" alt="image">
        <div class="card-body">
            <h6 class="card-subtitle">Discover</h6>
            <h5 class="card-title">Components</h5>
            <p class="card-text">
                Reusable components designed for the mobile interface and ready to use.
            </p>
            <a href="app-components.html" class="btn btn-danger"> --> <!-- -->
                <!-- <ion-icon name="cube-outline"></ion-icon>
                Preview
            </a>
        </div>
    </div>
</div> -->

<!-- <div class="section mt-3 mb-3">
    <div class="card">
        <img src="assets/img/sample/photo/wide2.jpg" class="card-img-top" alt="image">
        <div class="card-body">
            <h6 class="card-subtitle">Discover</h6>
            <h5 class="card-title">Pages</h5>
            <p class="card-text">
                Mobilekit comes with basic pages you may need and use in your projects easily.
            </p>
            <a href="app-pages.html" class="btn btn-danger"> --> <!-- -->
                <!-- <ion-icon name="layers-outline"></ion-icon>
                Preview
            </a>
        </div>
    </div>
</div> -->


<!-- app footer -->
<!-- <div class="appFooter">
    <img src="assets/img/logo.png" alt="icon" class="footer-logo mb-2">
    <div class="footer-title">
        Copyright © Mobilekit 2020. All Rights Reserved.
    </div>
    <div>Mobilekit is PWA ready Mobile UI Kit Template.</div>
    Great way to start your mobile websites and pwa projects.

    <div class="mt-2">
        <a href="javascript:;" class="btn btn-icon btn-sm btn-facebook">
            <ion-icon name="logo-facebook"></ion-icon>
        </a>
        <a href="javascript:;" class="btn btn-icon btn-sm btn-twitter">
            <ion-icon name="logo-twitter"></ion-icon>
        </a>
        <a href="javascript:;" class="btn btn-icon btn-sm btn-linkedin">
            <ion-icon name="logo-linkedin"></ion-icon>
        </a>
        <a href="javascript:;" class="btn btn-icon btn-sm btn-instagram">
            <ion-icon name="logo-instagram"></ion-icon>
        </a>
        <a href="javascript:;" class="btn btn-icon btn-sm btn-whatsapp">
            <ion-icon name="logo-whatsapp"></ion-icon>
        </a>
        <a href="#" class="btn btn-icon btn-sm btn-secondary goTop">
            <ion-icon name="arrow-up-outline"></ion-icon>
        </a>
    </div>

</div> -->
<!-- * app footer -->

<!-- </div> -->
<!-- * App Capsule -->