<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (!empty($_SESSION["userinfo_ID"]))
        jsRedirect("replace", "index.php");
    if (!empty($_GET["tel"]))
    {
        $tel = $_GET["tel"]; 
    }
    else
        $display_get_otp_button = "display: none;";
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body style = " background-image: url('img/materials/giver_background.png');
                background-repeat:no-repeat;
                background-size:cover;"><!-- class="bg-white"  -->>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->


    <!-- App Capsule -->
    <div id="appCapsule" class="pt-0">

        <div class="login-form mt-5 pt-5">
            <div class="section">
                <img src="img/materials/GIVER-LOGO-PNG.png" alt="image" class="form-image">
            </div>
            <div class="section mt-1">
                <h1>OTP <br>Password recovery</h1>
            </div>
            <div class="section mt-1 mb-5">
                <form   action="frontend_operation.php" method = "POST"
                        name = "reset_password" id = "reset_password">
                    <div style = "display: none;">
                        <input type = "text" name = "operation_command" id = "operation_command" value = "reset_password">
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" style = "border-radius: 35px;" class="form-control" name = "tel" id="tel" value = "<?=$tel?>" placeholder="Mobile number" onkeyup = "check_empty_tel()" required>
                            <i class="clear-input">
                                <ion-icon name="close-circle" onclick = "document.getElementById('get_otp_btn_div').style.display='none';"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <div class = "row">
                             <input type="text" style = "border-radius: 35px;" class="form-control" name = "otp" id="otp" placeholder="OTP" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                            <div class = "row mt-1" id = "get_otp_btn_div" style = "<?=$display_get_otp_button?>">
                                <input  type="button" class="btn btn-danger btn-block btn-lg" style = "border-radius: 35px;" 
                                        value = "Get OTP" onclick = "create_otp()">
                            </div>
                            <div class = "row mt-1" id = "submit_btn_div" style = "display: none;">
                                <input  type="button" class="btn btn-danger btn-block btn-lg" style = "border-radius: 35px;" 
                                        value = "Verify OTP" onclick = "form_submit()">
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-links mt-2">
                        <div class = "row w-100">
                            <button type="submit" class="btn btn-danger btn-block btn-lg" style = "border-radius: 35px;">Log in</button>
                        </div>
                    </div> -->

                    <!-- <div class="form-links mt-2">
                        <div>
                            <a href="page-register.html">Register Now</a>
                        </div>
                        <div><a href="page-forgot-password.html" class="text-muted">Forgot Password?</a></div>
                    </div>

                    <div class="form-button-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Log in</button>
                    </div> -->

                </form>
            </div>
        </div>


    </div>
    <!-- * App Capsule -->



    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

    <script src="assets/js/main_function.js"></script>

    <script>
        function check_empty_tel()
        {
            var tel_val = document.getElementById('tel').value;
            if (tel_val == "")
            {
                document.getElementById('get_otp_btn_div').style.display = "none";
            }
            else
            {
                document.getElementById('get_otp_btn_div').style.display = "";
            }
        }

        function switch_1 ()
        {
            document.getElementById("get_otp_btn_div").style.display = "none";
            document.getElementById("submit_btn_div").style.display = "";
        }
        
        async function create_otp ()
        {
            var tel_val = document.getElementById('tel').value;
            result = ajax_req("ajax_request.php", "operation_command=create_otp&username="+tel_val, after_ajax);
            function after_ajax(result)
            {
                /* if (result == "1")
                {
                    alert('สร้าง OTP เรียบร้อยแล้ว กรุณารอรับ OTP');
                }
                else
                {
                    alert('สร้าง OTP ไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');
                } */
                /* else if (result != "1")
                {
                    if ((password_1_val != "") && (password_2_val != "") && (password_1_val == password_2_val) && (tel_val != ""))
                    {
                        document.getElementById('username_exist').style.display = 'none';
                        document.getElementById('submit_div').style.display = '';
                    }
                } */
                if (result == "0")
                {
                    alert("ไม่พบหมายเลขโทรศัพท์ที่ระบุไว้ กรุณาลองใหม่อีกครั้ง");
                    document.getElementById("get_otp_btn_div").style.display = "";
                    document.getElementById("submit_btn_div").style.display = "none";
                }
                else if (result == "1")
                {
                    switch_1();
                }
            }
        }

        function form_submit()
        {
            document.getElementById("reset_password").submit();
        }
    </script>


</body>

</html>