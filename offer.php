<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    $page_header = "ข้อเสนอเพิ่มเติม";
    //alert(($_SESSION["userinfo_ID"]));
    $userinfo = get_userinfo($_SESSION["userinfo_ID"]);
    $product_arr = get_all_products();
    $back_target = "giver.php";
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

    <style>
        ion-icon {
        color: white;
        }
        * {box-sizing:border-box}

        /* Slideshow container */
        .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
        }

        /* Hide the images by default */
        .mySlides {
        display: none;
        }

        /* slide_slide_text & slide_previous buttons */
        .slide_prev, .slide_slide_text {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        }

        /* Position the "slide_slide_text button" to the right */
        .slide_slide_text {
        right: 0;
        border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .slide_prev:hover, .slide_slide_text:hover {
        background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
        }

        .slide_active, .dot:hover {
        background-color: #717171;
        }

        /* Fading animation */
        .slide_fade {
        -webkit-animation-name: slide_fade;
        -webkit-animation-duration: 1.5s;
        animation-name: slide_fade;
        animation-duration: 1.5s;
        }

        @-webkit-keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }

        @keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }
    </style>
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- Header -->
    <?php include 'section_materials/topbar_back.php';?>
    <!-- Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body py-2">
                    <div class="row w-100">
                        <div class="col-8 my-auto font-weight-bold text-dark">
                            ข้อเสนอเพิ่มเติม 1
                        </div>
                       <!--  
                            <div class="col-4">
                            <input type = "button" class = "btn btn-danger rounded" value = "อัปเกรดตอนนี้">
                            </div> 
                        -->
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body py-2">
                    <div class="row w-100">
                        <div class="col-8 my-auto font-weight-bold text-dark">
                            ข้อเสนอเพิ่มเติม 2
                        </div>
                       <!--  
                            <div class="col-4">
                            <input type = "button" class = "btn btn-danger rounded" value = "อัปเกรดตอนนี้">
                            </div> 
                        -->
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body py-2">
                    <div class="row w-100">
                        <div class="col-8 my-auto font-weight-bold text-dark">
                            ข้อเสนอเพิ่มเติม 3
                        </div>
                       <!--  
                            <div class="col-4">
                            <input type = "button" class = "btn btn-danger rounded" value = "อัปเกรดตอนนี้">
                            </div> 
                        -->
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body py-2">
                    <div class="row w-100">
                        <div class="col-8 my-auto font-weight-bold text-dark">
                            ข้อเสนอเพิ่มเติม 4
                        </div>
                       <!--  
                            <div class="col-4">
                            <input type = "button" class = "btn btn-danger rounded" value = "อัปเกรดตอนนี้">
                            </div> 
                        -->
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 font-weight-bold text-dark">
            ศูนย์การเรียนรู้ Giver
        </div>
        <hr>
         <div class="section mt-3 mb-3">
            <div class="card mb-2"  style = "background-color: #17a2b8;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <div class = "col-3 rounded text-white text-center">
                            <ion-icon name="school-outline" style = "font-size: 64px;"></ion-icon>
                            ศูนย์การเรียนรู้
                        </div>
                        <div class = "col-3 rounded text-white text-center">
                            <ion-icon name="stats-chart-outline" style = "font-size: 64px;"></ion-icon>
                            เติบโตอย่างไร
                        </div>
                        <div class = "col-3 rounded text-white text-center">
                            <ion-icon name="newspaper-outline" style = "font-size: 64px;"></ion-icon>
                            ข่าวล่าสุด
                        </div>
                        <div class = "col-3 rounded text-white text-center">
                            <ion-icon name="create-outline" style = "font-size: 64px;"></ion-icon>
                            FAQ
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
        <?php include 'section_materials/bottom_menu_3.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- welcome notification  -->
    <!-- <div id="notification-welcome" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="assets/img/icon/72x72.png" alt="image" class="imaged w24">
                    <strong>Mobilekit</strong>
                    <span>just now</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Welcome to Mobilekit</h3>
                    <div class="text">
                        Mobilekit is a PWA ready Mobile UI Kit Template.
                        Great way to start your mobile websites and pwa projects.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- * welcome notification -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


    <script>
        setTimeout(() => {
            notification('notification-welcome', 5000);
        }, 2000);
    </script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        // slide_slide_text/slide_previous controls
        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" slide_active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " slide_active";
        }

        function show_load ()
        {
            
            async function main_func() 
            {
                // task 1
                document.getElementById('loader').style.display = '';
                
                // tast 2
                const result = await new Promise (resolve => {$("#loader").fadeToggle(500); resolve(1);});
            }

            main_func();
        }
    </script>
</body>

</html>