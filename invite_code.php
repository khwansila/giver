<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (!empty($_SESSION["userinfo_ID"]))
        jsRedirect("replace", "index.php");
    if (!empty($_GET["tel"]))
        $owner_ID = $_GET["tel"];
    if (!empty($_GET["invite_code"]))
        $invite_code = $_GET["invite_code"];
    else
        $invite_code = "";
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body style = " background-image: url('img/materials/giver_background.png');
                background-repeat:no-repeat;
                background-size:cover;"><!-- class="bg-white"  -->>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->


    <!-- App Capsule -->
    <div id="appCapsule" class="pt-0">

        <div class="login-form mt-5 pt-5">
            <div class="section">
                <img src="img/materials/GIVER-LOGO-PNG.png" alt="image" class="form-image">
            </div>
            <div class="section mt-1">
                <h1>Invite code</h1>
                <h4>ใส่รหัส Invite code</h4>
            </div>
            <div class="section mt-1 mb-5">
                <form   action="check_invite_code.php" method = "POST"
                        name = "invite_code_form" id = "invite_code_form">
                    <div style = "display: none;">
                        <input type = "text" name = "operation_command" id = "operation_command" value = "check_invite_code">
                        <input type = "text" name = "owner_ID" id = "owner_ID" value = "<?=$owner_ID?>">
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input  type="text" style = "border-radius: 35px;" class="form-control" 
                                    name = "invite_code" id="invite_code" 
                                    value = "<?=$invite_code?>" placeholder="invite code"
                                    onkeyup = "check_invite_code()">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed" id = "result_pass_div" style = "display: none;">
                        <div class="input-wrapper text-center mx-auto">
                           <span style = "color:green;">รหัสแนะนำถูกต้อง</span>
                        </div>
                    </div>

                    <div class="form-group boxed" id = "result_failed_div" style = "display: none;">
                        <div class="input-wrapper text-center mx-auto">
                           <span style = "color: red;">รหัสแนะนำผิด</span>
                        </div>
                    </div>

                    <div class="form-links mt-2" id = "verify_invite_code_div" style = "display: none;">
                        <div class = "row w-100">
                            <input  type="button" class="btn btn-danger btn-block btn-lg" style = "border-radius: 35px;"
                                    value = "ตรวจสอบ Invite code" onclick = "show_confirm();
                check_invite_code_ajax ();">
                        </div>
                    </div>

                    <div class="form-links mt-2" id = "confirm_invite_code_div" style = "display: none;">
                        <div class = "row w-100">
                            <input  type="button" class="btn btn-danger btn-block btn-lg" style = "border-radius: 35px;"
                                    value = "ยืนยัน" onclick = "document.getElementById('invite_code_form').submit()">
                        </div>
                    </div>

                    <div class="form-links mt-2" id = "cancel_invite_code_div" style = "display: none;">
                        <div class = "row w-100">
                            <input  type="button" class="btn btn-outline-danger btn-block btn-lg" style = "border-radius: 35px;"
                                    value = "ยกเลิก" onclick = "document.getElementById('invite_code').value=''; hide_all();">
                        </div>
                    </div>

                    <!-- <div class="form-links mt-2">
                        <div class = "row w-100 text-center">
                            <div class = "col">
                                <a href="page-register.html">Forgot Password</a>
                            </div>
                        </div>
                    </div>

                    <div class="form-links mt-2">
                        <div class = "row w-100 text-center">
                            <div class = "col">
                                <a href="page-register.html">Register Now</a>
                            </div>
                        </div>
                    </div> -->

                    <!-- <div class="form-links mt-2">
                        <div>
                            <a href="page-register.html">Register Now</a>
                        </div>
                        <div><a href="page-forgot-password.html" class="text-muted">Forgot Password?</a></div>
                    </div>

                    <div class="form-button-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Log in</button>
                    </div> -->
                </form>
            </div>
        </div>


    </div>
    <!-- * App Capsule -->



    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

    <script src="assets/js/main_function.js"></script>

    <script>

        function show_verify()
        {
            document.getElementById("verify_invite_code_div").style.display = "";
            document.getElementById("confirm_invite_code_div").style.display = "none";
            document.getElementById("cancel_invite_code_div").style.display = "none";
        }

        function show_confirm()
        {
            document.getElementById("verify_invite_code_div").style.display = "none";
            document.getElementById("confirm_invite_code_div").style.display = "";
            document.getElementById("cancel_invite_code_div").style.display = "";
        }

        function hide_all()
        {
            document.getElementById("verify_invite_code_div").style.display = "none";
            document.getElementById("confirm_invite_code_div").style.display = "none";
            document.getElementById("cancel_invite_code_div").style.display = "none";
            document.getElementById('result_pass_div').style.display = 'none';
            document.getElementById('result_failed_div').style.display = 'none';
        }

        function check_invite_code()
        {
            var invite_code = document.getElementById('invite_code').value ;
            if (invite_code != "")
            {
                show_verify();
            }
            else
            {
                hide_all();
            }

        }

        async function check_invite_code_ajax ()
        {
            var invite_code = document.getElementById('invite_code').value;
            var result = ajax_req("ajax_request.php", "operation_command=check_invite_code&invite_code="+invite_code, after_ajax);
            function after_ajax(result)
            {
                if (result == "1")
                {
                    document.getElementById('result_pass_div').style.display = '';
                    document.getElementById('result_failed_div').style.display = 'none';
                }
                else if (result != "1")
                {
                    document.getElementById('result_pass_div').style.display = 'none';
                    document.getElementById('result_failed_div').style.display = '';
                }
            }
        }
    </script>
</body>

</html>