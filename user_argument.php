<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    //alert(($_SESSION["userinfo_ID"]));
    $cart = get_cart ($_SESSION["userinfo_ID"]);
    $product_ID_list = $cart[0]["product_ID_list"];
    $product_ID_arr = explode(",", $cart[0]["product_ID_list"]);
    $product_detail_arr = get_product_detail_from_list ($product_ID_list);
    $quantity_arr = explode(",", $cart[0]["quantity_list"]);
    $price_arr = explode(",", $cart[0]["price_list"]);
    $val = rand(1,99999999);
    $result_get_order = get_order ($_SESSION["userinfo_ID"]);
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-danger text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">รายการสั่งซื้อ</div>
        <!-- <div class="right">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#DialogClear">
                <ion-icon name="trash-outline"></ion-icon>
            </a>
        </div> -->
    </div>
    <!-- * App Header -->

    <!-- Dialog Clear -->
    <div class="modal fade dialogbox" id="DialogClear" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Empty Cart</h5>
                </div>
                <div class="modal-body">
                    All items will be deleted.
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CLOSE</a>
                        <a href="#" class="btn btn-text-danger" data-dismiss="modal">DELETE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * Dialog Clear -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div class = "section mt-5">
            <b><h2>นโยบายของเรา</h2></b><br>
                <p>
                    ข้อ 1 ความยินยอมของผู้ใช้งาน ในการเข้าใช้งานเว็บไซต์และแอปพลิเคชัน ผู้ใช้งานตกลงและให้ความยินยอมเกี่ยวกับการเก็บรวบรวมและใช้ข้อมูลส่วนบุคคล รวมถึงการส่งหรือโอนข้อมูลส่วนบุคคลไปยังต่างประเทศ ดังต่อไปนี้(ก) วัตถุประสงค์แห่งการเก็บรวบรวมและใช้ข้อมูลส่วนบุคคล รวมถึงการส่งหรือโอนข้อมูลส่วนบุคคลไปยังต่างประเทศ ผู้ใช้งานรับทราบ ตกลง และยินยอมให้ผู้ควบคุมข้อมูลและผู้ประมวลผลข้อมูลเก็บรวบรวมและใช้ข้อมูลส่วนบุคคล เพื่อวัตถุประสงค์ดังต่อไปนี้เท่านั้นการประชาสัมพันธ์ และ การตลาด, การให้บริการสื่อสังคม และ การเรียกเก็บเงินค่าบริการ<br><br>
                    ข้อ 2 การเชื่อมโยงข้อมูลของผู้ใช้งานเว็บไซต์และแอปพลิเคชันกับผู้ให้บริการบุคคลที่สาม ผู้ใช้งานรับทราบ ตกลง และยินยอมให้ผู้ควบคุมข้อมูลอาจการเชื่อมโยงข้อมูลของผู้ใช้งานเว็บไซต์และแอปพลิเคชันกับผู้ให้บริการบุคคลที่สาม โดยในการเชื่อมโยงหรือแบ่งปันข้อมูลต่อผู้ให้บริการบุคคลที่สามในแต่ละคราว ผู้ควบคุมข้อมูลจะแจ้งให้ผู้ใช้งานทราบว่าข้อมูลของผู้ใช้งานใดที่จะถูกเชื่อมโยงหรือแบ่งปันแก่ผู้ให้บริการบุคคลที่สาม ทั้งนี้ เมื่อผู้ใช้งานได้แสดงเจตนาโดยชัดแจ้งในการอนุญาตให้มีการเชื่อมโยงหรือแบ่งปันดังกล่าวนั้น อันรวมถึงแต่ไม่จำกัดเพียง การกดยอมรับ อนุญาต เชื่อมโยง แบ่งปันหรือการกระทำใดๆ อันมีลักษณะโดยชัดแจ้งว่าผู้ใช้งานได้ยินยอมในการเชื่อมโยงหรือแบ่งปันข้อมูลต่อผู้ให้บริการบุคคลที่สามนั้น<br><br>
                    ข้อ 3 การติดตามพฤติกรรมการใช้งานเว็บไซต์และแอปพลิเคชันของผู้ใช้งานผู้ใช้งานรับทราบ ยินยอม และตกลงให้ผู้ควบคุมข้อมูลอาจใช้ระบบและ/หรือเทคโนโลยีดังต่อไปนี้ในการติดตามพฤติกรรมการใช้งานเว็บไซต์และแอปพลิเคชันของผู้ใช้งานเทคโนโลยี Cookies, การใช้งาน Pixel Tags และ Google Analytics Tag ทั้งนี้ เพื่อวัตถุประสงค์ดังระบุต่อไปนี้เท่านั้นเพื่อพัฒนาการให้บริการ และ การนำเสนอสินค้าที่ตรงความต้องการของผู้ใช้งาน<br><br>
                    ข้อ 4 การถอนความยินยอมของผู้ใช้งานผู้ใช้งานรับทราบว่าผู้ใช้งานมีสิทธิที่จะถอนความยินยอมใดๆ ที่ผู้ใช้งานได้ให้ไว้แก่ผู้ควบคุมข้อมูลตามนโยบายฉบับนี้ได้ ไม่ว่าเวลาใดโดยการดำเนินการ ดังต่อไปนี้แจ้งเป็นลายลักษณ์อักษรที่อีเมล sapappsth.biz@gmail.com หรือ เลือก “ไม่ยินยอม” เมื่อลงทะเบียน<br><br>
                    ข้อ 5 บัญชีผู้ใช้ในการใช้งานเว็บไซต์และแอปพลิเคชัน ผู้ควบคุมข้อมูลอาจจัดให้มีบัญชีผู้ใช้ของแต่ละผู้ใช้งานเพื่อการใช้งานเว็บไซต์และแอปพลิเคชัน โดยที่ผู้ควบคุมข้อมูลมีสิทธิแต่เพียงผู้เดียวในการอนุมัติเปิดบัญชีผู้ใช้ กำหนดประเภทบัญชีผู้ใช้ กำหนดสิทธิการเข้าถึงข้อมูลของแต่ละประเภทบัญชีผู้ใช้ สิทธิการใช้งานเว็บไซต์และแอปพลิเคชัน ค่าใช้จ่ายใดๆ เกี่ยวกับบัญชีผู้ใช้ หน้าที่และความรับผิดชอบของผู้ใช้งานซึ่งเป็นเจ้าของบัญชีผู้ใช้นั้นๆ ทั้งนี้ ผู้ใช้งานตกลงจะเก็บรักษาชื่อบัญชีผู้ใช้ รหัสผ่าน และข้อมูลใดๆ ของตนไว้เป็นความลับอย่างเคร่งครัด และตกลงจะไม่ยินยอมให้ รวมถึงใช้ความพยายามอย่างที่สุดในการป้องกันไม่ให้บุคคลอื่นใช้งานบัญชีผู้ใช้ของผู้ใช้งานในกรณีที่มีการใช้บัญชีผู้ใช้ของผู้ใช้งานโดยบุคคลอื่น ผู้ใช้งานตกลงและรับรองว่าการใช้งานโดยบุคคลอื่นดังกล่าวได้กระทำในฐานะตัวแทนของผู้ใช้งานและมีผลผูกพันเสมือนหนึ่งผู้ใช้งานเป็นผู้กระทำการเองทิ้งสิ้น<br><br>
                    ข้อ 6 สิทธิของผู้ใช้งานในการเข้าใช้งานเว็บไซต์และแอปพลิเคชันตามนโยบายฉบับนี้และการให้ความยินยอมใดๆ ตามนโยบายฉบับนี้ ผู้ใช้งานได้รับทราบถึงสิทธิของตนในฐานะเจ้าของข้อมูลส่วนบุคคลตามกฎหมายคุ้มครองข้อมูลส่วนบุคคลเป็นอย่างดีแล้ว อันรวมถึงแต่ไม่จำกัดเพียงสิทธิของผู้ใช้งาน<br><br>
                    ข้อ 7 การรักษาความมั่นคงปลอดภัย ในการเก็บรวบรวมและใช้ข้อมูลส่วนบุคคลตามนโยบายฉบับนี้ ผู้ควบคุมข้อมูลจะจัดให้มีมาตรการรักษาความมั่นคงปลอดภัยที่เหมาะสมเพื่อป้องกันการสูญหาย การเข้าถึง ใช้ เปลี่ยนแปลง แก้ไข หรือการเปิดเผยข้อมูลที่ไม่เป็นไปตามกฎมหาย ด้วยมาตรการ มาตรฐาน เทคโนโลยีและ/หรือด้วยระบบ ดังต่อไปนี้กำหนดสิทธิ์การเข้าถึงข้อมูล (Access Right), การเข้ารหัสข้อมูล (Encryption) และ ระบบ Firewallsรวมถึงการควบคุมให้ผู้ประมวลผลข้อมูลมีการรักษาความมั่นคงปลอดภัยของข้อมูลส่วนบุคคลที่ไม่น้อยไปกว่าที่กำหนดในนโยบายฉบับนี้ด้วย<br><br>
                </p>
        </div>
    </div>
    <!-- * App Capsule -->

    <!-- App Bottom Menu -->
    <?php include 'section_materials/bottom_menu.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

    <script src="assets/js/main_function.js"></script>

    <script>
        async function delete_cart_item (position, product_position)
        {
            //var quantity = document.getElementById('quantity').value;
            result = ajax_req("ajax_request.php", "operation_command=delete_cart_item&owner_ID=<?=$_SESSION["userinfo_ID"]?>&position="+position, after_ajax);
            async function after_ajax(result, update_net_price)
            {
               if (result == "1")
                {
                    //alert('ใส่สินค้าลงตะกร้าเรียบร้อยแล้ว');
                    document.getElementById(product_position).style.display = "none";
                    ajax_req("ajax_request.php", "operation_command=update_net_price&owner_ID=<?=$_SESSION["userinfo_ID"]?>", update_net_price);
                }
                else
                {
                    //alert('ใส่สินค้าลงตะกร้าไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');
                }
                /* else if (result != "1")
                {
                    if ((password_1_val != "") && (password_2_val != "") && (password_1_val == password_2_val) && (tel_val != ""))
                    {
                        document.getElementById('username_exist').style.display = 'none';
                        document.getElementById('submit_div').style.display = '';
                    }
                } */ 
                function update_net_price (val)
                {
                    document.getElementById('net_price').innerHTML = val;
                    document.getElementById('total').value = val.replace(",","")
                }
            }
           
        }
    </script>

</body>

</html>