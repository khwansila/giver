<?php
    function jsRedirect ($method, $url)
    {
        echo "<script>window.location.$method('$url')</script>";
        exit(0);
    }

    function jsBack()
    {
        echo "<script>window.history.back()</script>";
        exit(0);
    }

    function alertBack($msg)
    {
        alert($msg);
        jsBack();
    }

    function alertGoto ($msg, $url)
    {
        alert($msg);
        jsRedirect("replace", $url);
    }

    function cut_quote ($msg)
    {
        return str_replace("\"", "", str_replace("'", "", $msg));
    }

    function replace_backslash_quote ($msg)
    {
        return str_replace('"', '\"', str_replace("'", "\'", $msg));
    }

    function cut_last_comma ($msg)
    {
        return substr_replace($msg ,"",-1);
    }

    function sql_val ($val)
    {
        $val_sql = (empty($val) && ($val != 0) & ($val != "0")) ? "NULL" : "'$val'";
        return $val_sql;
    }

    function insert_error ($error_sql, $checkpoint)
    {
        $error_msg = cut_quote(mysqli_error($GLOBALS["con"]));
		//$error_msg = "test";
        $error_sql = cut_quote($error_sql);
        $checkpoint = cut_quote($checkpoint);
        $sql = "INSERT INTO tb_error_log (msg, checkpoint, sql_statement) VALUES ('$error_msg', '$checkpoint', '$error_sql');";
		$result = mysqli_query($GLOBALS["con"], $sql);
    }

    function reserve_row ($table_name, $column_name)
    {
        $return_result = 0;
        $uid = rand(100000,999999999);
        $sql = "INSERT INTO $table_name ($column_name) VALUES ('$uid');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
        {
            //return reserve_row ($table_name, $column_name);
            echo mysqli_error($GLOBALS["con"]);
            echo "<br>".$sql;
            //alertBack("ไม่สามารถดำเนินการได้ กรุณาลองใหม่อีกครั้ง (1)");
            exit(0);
        }
            
        else
        {
            $sql = "SELECT ID FROM $table_name WHERE $column_name = '$uid' ;";
            $result = mysqli_query($GLOBALS["con"], $sql);
            if ($result->num_rows > 0)
            {
                if ($row = $result->fetch_assoc())
                {
                    $return_result = $row["ID"];
                }
            }
        }
        return $return_result;
    }

    function get_userinfo_ID_from_invite_code ($invite_code)
    {
        $return_result = 0;
        //$sql = "SELECT owner_ID FROM tb_invite_code WHERE code = '$invite_code';";
        $sql = "SELECT ID FROM tb_login WHERE username = '$invite_code';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $login_ID = $row["ID"];
            }
        }

        $sql = "";
        $result = "";

        $sql = "SELECT ID FROM tb_userinfo WHERE login_ID = '$login_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row["ID"];
            }
        }
        return $return_result;
    }
    
    function get_userinfo ($userinfo_ID)
    {
        $return_result_arr = array();
        //$sql = "SELECT owner_ID FROM tb_invite_code WHERE code = '$invite_code';";
        $sql = "SELECT tb_userinfo.*, tb_login.username FROM tb_userinfo LEFT JOIN tb_login ON tb_userinfo.login_ID = tb_login.ID WHERE tb_userinfo.ID = '$userinfo_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function check_exist_username ($username)
    {
        $return_result = "";
        $sql_get_username = "SELECT username FROM tb_login WHERE username = '$username';";
        $result_get_username = mysqli_query($GLOBALS["con"], $sql_get_username);
        if ($result_get_username->num_rows > 0)
            $return_result = "1";
        else
            $return_result = "0";
        //return $return_result;
        return $return_result;
    }

    function set_new_password ($username, $password)
    {
        $sql_update_new_password = "UPDATE tb_login SET password = '$password' WHERE username = '$username';";
        $result = mysqli_query($GLOBALS["con"], $sql_update_new_password);
        return $result;
    }

    function insert_suggest ($owner_ID, $suggest_ID)
    {
        $sql = "INSERT INTO tb_suggest (owner_ID, suggest_ID) VALUES ('$owner_ID', '$suggest_ID');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function insert_new_blank_cart($owner_ID)
    {
        $sql = "INSERT INTO tb_cart (owner_ID) VALUES ('$owner_ID');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function insert_login ($username, $password)
    {
        $return_result = 0;
        $sql = "INSERT INTO tb_login (username, password) VALUES ('$username', '$password');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            return 0;
        else
        {
            $sql = "SELECT ID FROM tb_login WHERE username = '$username';";
            $result = mysqli_query($GLOBALS["con"], $sql);
            if ($result->num_rows > 0)
            {
                if ($row = $result->fetch_assoc())
                {
                    $return_result = $row["ID"];
                }
            }
        }
        return $return_result;
    }

    function insert_new_user ($username, $password)
    {
        $userinfo_ID = reserve_row ("tb_userinfo", "login_ID");
        $login_ID = insert_login ($username, $password);

        $sql_update_userinfo = "UPDATE tb_userinfo SET login_ID = '$login_ID' WHERE ID = '$userinfo_ID';";
        $result_update_userinfo = mysqli_query($GLOBALS["con"], $sql_update_userinfo);

        $result = insert_new_blank_cart($userinfo_ID);
        return $result;
    }

    function check_otp ($username, $otp)
    {
        $return_result = 0;
        $value = "";
        $sql = "SELECT value FROM tb_otp WHERE username = '$username' ORDER BY edited_date DESC LIMIT 1;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $value = $row["value"];
            }
        }
        if ($value == $otp)
        {
            $return_result = 1;
        }
        return $return_result;
    }

    function insert_stock_log ($product_ID, $quantity, $operation)
    {
        $product_ID = !empty($product_ID) ? "'".cut_quote($product_ID)."'" : "NULL";
        $quantity = !empty($quantity) ? "'".cut_quote($quantity)."'" : "NULL";
        $operation = !empty($operation) ? "'".cut_quote($operation)."'" : "NULL";
        $created_by = "'".cut_quote($_SESSION["userinfo_ID"])."'";
        $sql = "INSERT INTO tb_stock_log (product_ID, quantity, operation, created_by, created_date) VALUES ($product_ID, $quantity, $operation, $created_by, NOW())";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }
    
    function get_ID ($column_name, $tb_name, $column_key, $value_key)
    {
        $sql = "SELECT $column_name FROM $tb_name WHERE $column_key = '$value_key';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row[$column_name];
            }
        }
        return $return_result;
    }

    function add_product($barcode, $name, $description, $spec, $price, $stock)
    {
        $product_ID = reserve_row("tb_product", "name");
        insert_stock_log($product_ID, $stock, "เพิ่มสินค้า");
        $barcode = !empty($barcode) ? "'".cut_quote($ID)."'" : "NULL";
        $name = !empty($name) ? "'".cut_quote($name)."'" : "NULL";
        $description = !empty($description) ? "'".cut_quote($description)."'" : "NULL";
        $spec = !empty($spec) ? "'".cut_quote($spec)."'" : "NULL";
        $price = !empty($price) ? "'".cut_quote($price)."'" : "NULL";
        $stock = !empty($stock) ? "'".cut_quote($stock)."'" : "NULL";
        $created_by = "'".cut_quote($_SESSION["userinfo_ID"])."'";
        $sql = "
                UPDATE  tb_product
                SET     barcode = $barcode,
                        name = $name,
                        description = $description,
                        spec = $spec,
                        price = $price,
                        stock = $stock,
                        created_by = $created_by
                WHERE   ID = '$product_ID';
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            insert_error("add_product", $sql);
        return $result;
    }

    function add_voucher ($barcode, $name, $description, $spec, $price, $stock, $store_ID)
    {

        $product_ID = reserve_row("tb_product", "name");
        insert_stock_log($product_ID, $stock, "เพิ่มสินค้า");
        $barcode = !empty($barcode) ? "'".cut_quote($ID)."'" : "NULL";
        $name = !empty($name) ? "'".cut_quote($name)."'" : "NULL";
        $description = !empty($description) ? "'".cut_quote($description)."'" : "NULL";
        $spec = !empty($spec) ? "'".cut_quote($spec)."'" : "NULL";
        $price = !empty($price) ? "'".cut_quote($price)."'" : "NULL";
        $stock = !empty($stock) ? "'".cut_quote($stock)."'" : "NULL";
        $store_ID = !empty($store_ID) ? "'".cut_quote($store_ID)."'" : "NULL";
        $created_by = "'".cut_quote($_SESSION["userinfo_ID"])."'";
        $sql = "
                UPDATE  tb_product
                SET     barcode = $barcode,
                        name = $name,
                        description = $description,
                        spec = $spec,
                        price = $price,
                        stock = $stock,
                        created_by = $created_by
                WHERE   ID = '$product_ID';
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            insert_error("add_product", $sql);

        for ($i = 1; $i <= cut_quote($stock); $i++)
        {
            $voucher_ID = reserve_row("tb_voucher_usage", "uid");
            $voucher_serial = uniqid().uniqid();
            $sql .= "UPDATE tb_voucher_usage SET product_ID = '$product_ID', serial = '$voucher_key', store_ID = '$store_ID', created_by = '$created_by' WHERE uid = '$voucher_ID';";
            //$sql = "UPDATE tb_voucher_usage SET product_ID = '$product_ID' , created_by = $created_by WHERE ID = '$voucher_ID';";
            //$result = mysqli_query($GLOBALS["con"], $sql);
        }
        if (!empty($sql))
        {
            $result = mysqli_multi_query($GLOBALS["con"], $sql);
            if (!($result))
            insert_error("add_voucher", $sql);
        }
        return $result;
    }

    function get_all_products ()
    {
        $return_result_arr = array();
        $sql = "SELECT tb_product.*, tb_bpoint.bpoint, tb_gpoint.gpoint
                FROM tb_product
                LEFT JOIN tb_bpoint ON tb_product.ID = tb_bpoint.product_ID
                LEFT JOIN tb_gpoint ON tb_product.ID = tb_gpoint.product_ID
                WHERE tb_product.status = '1' and type = '1';
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_bpoint_exchange($product_ID)
    {
        $return_result = "";
        $sql = "SELECT bpoint_exchange FROM tb_product WHERE ID = '$product_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row["bpoint_exchange"];
            }
        }
        return $return_result == "" ? 0 : $return_result;
    }

    function get_gpoint_exchange($product_ID)
    {
        $return_result = "";
        $sql = "SELECT gpoint_exchange FROM tb_product WHERE ID = '$product_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row["gpoint_exchange"];
            }
        }
        return $return_result == "" ? 0 : $return_result;
    }

    function get_product_by_ID ($product_ID)
    {
        $return_result_arr = array();
        $sql = "SELECT tb_product.*, tb_bpoint.bpoint, tb_gpoint.gpoint
                FROM tb_product
                LEFT JOIN tb_bpoint ON tb_product.ID = tb_bpoint.product_ID
                LEFT JOIN tb_gpoint ON tb_product.ID = tb_gpoint.product_ID
                WHERE tb_product.ID = '$product_ID';
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_userinfo_list ()
    {
        $return_result = "";
        $sql = "SELECT ID FROM tb_userinfo WHERE 1;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $return_result .= $row["ID"].",";
            }
        }
        $return_result = cut_last_comma ($return_result);
        return $return_result;
    }

    function get_cart ($owner_ID)
    {
        $return_result_arr = array();
        $sql = "SELECT * FROM tb_cart WHERE owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                array_push ($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function update_cart ($owner_ID, $product_ID_list, $quantity_list, $price_list)
    {
        $return_result = 0;
        $sql = "UPDATE tb_cart SET product_ID_list = '$product_ID_list', quantity_list = '$quantity_list', price_list = '$price_list' WHERE owner_ID = '$owner_ID' ;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result)
            $return_result = 1;
        return $return_result;
    }

    function get_product_detail_from_list ($product_ID_list)
    {
        $return_result_arr = array();
        $product_ID_arr = explode(",", $product_ID_list);
        $product_arr = array();
        $sql = "SELECT *  FROM tb_product WHERE ID in ($product_ID_list); ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($product_arr, $row);
            }
        }
        $i = 0;
        while ($product_ID_arr[$i] != "")
        {
            $j = 0;
            while ($product_arr[$j]["ID"] != "")
            {
                if ($product_arr[$j]["ID"] == $product_ID_arr[$i])
                    array_push($return_result_arr, $product_arr[$j]);
                $j++;    
            }
            $i++;
        }
        return $return_result_arr;
    }

    function get_net_value_cart ($owner_ID)
    {
        $return_result = 0;
        $sql = "SELECT price_list FROM tb_cart WHERE owner_ID = '$owner_ID'; ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $price_list = $row["price_list"];
            }
        }
        $price_list_arr = explode (",", $price_list);
        $i = 0;
        while ($price_list_arr[$i] != "")
        {
            $return_result += $price_list_arr[$i];
            $i++;
        }
        return $return_result;
    }

    function create_order ($owner_ID, $ref_no, $product_ID_list, $quantity_list, $price_list, $net_price, $purchased_price)
    {
        $sql = "INSERT INTO tb_order (owner_ID, ref_no, product_ID_list, quantity_list, price_list, net_price, purchased_price, purchased_datetime, purchased_date, created_by, created_date) 
                VALUES ('$owner_ID', '$ref_no', '$product_ID_list', '$quantity_list', '$price_list', '$net_price', '$purchased_price', NOW(), NOW(), '".$_SESSION["userinfo_ID"]."', NOW());";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            $result = mysqli_error($GLOBALS["con"]);
        return $result;
    }

    function clear_cart($owner_ID)
    {
        $sql = "UPDATE tb_cart SET product_ID_list = NULL, quantity_list = NULL, price_list = NULL WHERE owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            $result = mysqli_error($GLOBALS["con"]);
        return $result;
    }

    function get_order ($owner_ID)
    {
        $return_result_arr = array();
        $sql = "SELECT * FROM tb_order WHERE owner_ID = '$owner_ID' ORDER BY edited_date ASC;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function regis_business($owner_ID)
    {
        $sql = "INSERT INTO tb_b_regis (owner_ID, regis_datetime, regis_date, created_by, created_date) 
                VALUES ('$owner_ID', NOW(), NOW(), '".$_SESSION["userinfo_ID"]."', NOW()); ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function calculate_b_point($product_ID, $quantity)
    {
        $return_result = 0;
        $sql_get_bpoint = "SELECT bpoint*$quantity AS bpoint FROM tb_bpoint WHERE product_ID = '$product_ID';";
        $result_get_bpoint = mysqli_query($GLOBALS["con"], $sql_get_bpoint);
        if ($result_get_bpoint->num_rows > 0)
        {
            while ($row = $result_get_bpoint->fetch_assoc())
            {
                $return_result = $row["bpoint"];
            }
        }
        return empty($return_result) ? 0 : $return_result;
    }

    /* function test_cal_b_point ($product_ID, $quantity)
    {
        $return_result = 0;
        $sql_get_bpoint = "SELECT bpoint*$quantity AS bpoint FROM tb_bpoint WHERE product_ID = '$product_ID';";
        $result_get_bpoint = mysqli_query($GLOBALS["con"], $sql_get_bpoint);
        if ($result_get_bpoint->num_rows > 0)
        {
            while ($row = $result_get_bpoint->fetch_assoc())
            {
                $return_result = $row["bpoint"];
            }
        }
        return empty($return_result) ? 0 : $return_result;
    } */

    function calculate_g_point($product_ID, $quantity)
    {
        $result = 0;
        $sql = "SELECT gpoint*$quantity AS gpoint FROM tb_gpoint WHERE product_ID = '$product_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            (print_r($result->fetch_assoc()));
            while ($row = $result->fetch_assoc())
            {
                $result = $row["gpoint"];
            }
        }
       /*  if (!($result))
            insert_error($sql, "cal_g_point"); */
        return $result ;
    }

    function insert_b_point_by_ref_no ($owner_ID, $point, $ref_no)
    {
        $created_by = $_SESSION["userinfo_ID"];
        $sql = "INSERT INTO tb_user_bpoint (owner_ID, point, ref_no, created_by, created_date) VALUES ('$owner_ID', '$point', '$ref_no', '$created_by', NOW());";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            //insert_error($sql, "insert b point by ref no");
            echo mysqli_error($GLOBALS["con"])."<br>".$sql;
        return $result;
    }

    function insert_g_point_by_ref_no ($owner_ID, $point, $ref_no)
    {
        $created_by = $_SESSION["userinfo_ID"];
        $sql = "INSERT INTO tb_user_gpoint (owner_ID, point, ref_no, created_by, created_date) VALUES ('$owner_ID', '$point', '$ref_no', '$created_by', NOW());";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function update_userinfo ($owner_ID, $name, $lastname, $gender, $birthdate)
    {
        $sql = "UPDATE  tb_userinfo
                SET     name = '$name',
                        lastname = '$lastname',
                        gender = '$gender',
                        birthdate = '$birthdate'
                WHERE   ID = '$owner_ID'; ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if (!($result))
            insert_error($sql, "update_userinfo");
        else
            alert("บันทึกข้อมูลสำเร็จ");
        return $result;
    }

    function get_address($owner_ID)
    {
        $return_result_arr = array();
        $sql = "SELECT  tb_address.*, 
                        tb_zone.zone_subdistrict AS subdistrict, 
                        tb_zone.zone_district AS district, 
                        tb_zone.zone_province AS province, 
                        tb_zone.zone_postcode AS postcode 
                FROM    tb_address 
                LEFT JOIN tb_zone ON tb_address.zone_ID = tb_zone.zone_ID 
                WHERE owner_ID = '$owner_ID'; ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr == array() ? 0 : $return_result_arr;
    }

    function insert_address ($owner_ID, $detail, $zone_ID)
    {
        $sql = "INSERT INTO tb_address (owner_ID, detail, zone_ID) VALUES ('$owner_ID', '$detail', $zone_ID);";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function update_address ($owner_ID, $detail, $zone_ID)
    {
        $sql = "UPDATE  tb_address 
                SET     detail = '$detail',
                        zone_ID = '$zone_ID'
                WHERE   owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function check_product_voucher ($product_ID)
    {
        $return_result = 0;
        $sql = "SELECT ID FROM tb_voucher_usage WHERE product_ID = '$product_ID' AND owner_ID IS NULL LIMIT 1;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = 1;
            }
        }
        return $return_result;
    }

    function insert_owner_voucher ($owner_ID, $product_ID, $quantity, $point_type, $point_value)
    {
        $voucher_uid_arr = array();
        $sql_select_voucher = "SELECT uid FROM tb_voucher_usage WHERE product_ID = '$product_ID' AND owner_ID IS NULL LIMIT $quantity;";
        $result_select_voucher = mysqli_query($GLOBALS["con"], $sql_select_voucher);
        if ($quantity > $result_select_voucher->num_rows)
        {
            alert("จำนวน Voucher ไม่เพียงพอ");
        }
        if ($result_select_voucher->num_rows > 0)
        {  
            while ($row = $result_select_voucher->fetch_assoc())
            {
                array_push($voucher_uid_arr, $row["uid"]);
            }
        }
        $i = 0;
        while ($voucher_uid_arr[$i] != "")
        {
            $sql_insert_owner .= "UPDATE tb_voucher_usage SET owner_ID = '$owner_ID', purchase_date = NOW() WHERE uid = '$voucher_uid_arr[$i]';";
            //echo $sql_insert_owner."<br>";
            $i++;
        }
        if ($sql_insert_owner != "")
        {
            $result = mysqli_multi_query($GLOBALS["con"], $sql_insert_owner);
            if ($point_type == "b")
            {
                insert_b_point_by_ref_no ($owner_ID, $point_value*(-1), "reward_exchange");
            }
            else if ($point_type == "g")
            {
                insert_g_point_by_ref_no ($owner_ID, $point_value*(-1), "reward_exchange");
            }
        }
            
        if (!($result))
            echo "<br>".mysqli_error($GLOBALS["con"]);

        return $result;
    }

    function get_voucher_info ($uid)
    {
        $return_result = array();
        $sql = "SELECT tb_voucher_usage.*, tb_product.name, tb_product.description, tb_product.gpoint_exchange, tb_product.bpoint_exchange, tb_userinfo.name, tb_userinfo.lastname, tb_login.username
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                LEFT JOIN tb_userinfo ON tb_voucher_usage.owner_ID = tb_userinfo.ID
                LEFT JOIN tb_login ON tb_userinfo.login_ID = tb_login.ID
                WHERE tb_voucher_usage.uid = '$uid';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
        return $return_result;
    }

    function use_voucher ($uid)
    {
        $sql = "UPDATE tb_voucher_usage SET status = '1', use_date = NOW() WHERE uid = '$uid';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function get_unuse_voucher_by_store_ID ($store_ID)
    {
        $return_result = array();
        $sql = "SELECT  tb_voucher_usage.*, tb_product.name AS product_name, tb_product.description AS product_description, 
                        tb_userinfo.name AS userinfo_name, tb_userinfo.lastname AS userinfo_lastname,
                        tb_login.username AS tel
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                LEFT JOIN tb_userinfo ON tb_userinfo.ID = tb_voucher_usage.owner_ID
                LEFT JOIN tb_login ON tb_login.ID = tb_userinfo.login_ID
                WHERE tb_voucher_usage.store_ID = '$store_ID' AND tb_voucher_usage.status = 0 ;";
        //echo $sql;
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
       // print_r ($return_result);
        return $return_result;
    }

    function get_unuse_voucher_by_store_ID_where ($store_ID, $where)
    {
        $return_result = array();
        $sql = "SELECT  tb_voucher_usage.*, tb_product.name AS product_name, tb_product.description AS product_description, 
                        tb_userinfo.name AS userinfo_name, tb_userinfo.lastname AS userinfo_lastname,
                        tb_login.username AS tel
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                LEFT JOIN tb_userinfo ON tb_userinfo.ID = tb_voucher_usage.owner_ID
                LEFT JOIN tb_login ON tb_login.ID = tb_userinfo.login_ID
                WHERE tb_voucher_usage.store_ID = '$store_ID' AND tb_voucher_usage.status = 0 $where ;";
        //echo $sql;
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
        //print_r ($return_result);
        return $return_result;
    }

    function get_used_voucher_by_store_ID($store_ID)
    {
        $return_result = array();
        $sql = "SELECT  tb_voucher_usage.*, tb_product.name AS product_name, tb_product.description AS product_description, 
                        tb_userinfo.name AS userinfo_name, tb_userinfo.lastname AS userinfo_lastname,
                        tb_login.username AS tel
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                LEFT JOIN tb_userinfo ON tb_userinfo.ID = tb_voucher_usage.owner_ID
                LEFT JOIN tb_login ON tb_login.ID = tb_userinfo.login_ID
                WHERE tb_voucher_usage.store_ID = '$store_ID' AND tb_voucher_usage.status = 1 ;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
        return $return_result;
    }

    function get_used_voucher_by_store_ID_where($store_ID, $where)
    {
        $return_result = array();
        $sql = "SELECT  tb_voucher_usage.*, tb_product.name AS product_name, tb_product.description AS product_description, 
                        tb_userinfo.name AS userinfo_name, tb_userinfo.lastname AS userinfo_lastname,
                        tb_login.username AS tel
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                LEFT JOIN tb_userinfo ON tb_userinfo.ID = tb_voucher_usage.owner_ID
                LEFT JOIN tb_login ON tb_login.ID = tb_userinfo.login_ID
                WHERE tb_voucher_usage.store_ID = '$store_ID' AND tb_voucher_usage.status = 1 $where ;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
        return $return_result;
    }

    function get_available_voucher_by_owner ($owner_ID)
    {
        $return_result = array();
        $sql = "SELECT tb_voucher_usage.*, tb_product.name, tb_product.description, tb_product.pic_url, tb_product.bpoint_exchange, tb_product.gpoint_exchange
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                WHERE tb_voucher_usage.owner_ID = '$owner_ID' AND tb_voucher_usage.status = 0";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
        return $return_result;
    }

    function get_used_voucher_by_owner ($owner_ID)
    {
        $return_result = array();
        $sql = "SELECT tb_voucher_usage.*, tb_product.name, tb_product.description, tb_product.pic_url, tb_product.bpoint_exchange, tb_product.gpoint_exchange
                FROM tb_voucher_usage 
                LEFT JOIN tb_product ON tb_voucher_usage.product_ID = tb_product.ID
                WHERE tb_voucher_usage.owner_ID = '$owner_ID' AND tb_voucher_usage.status = 1";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result, $row);
            }
        }
        return $return_result;
    }

    function get_bpoint ($owner_ID)
    {
        $return_result = 0;
        $sql = "SELECT SUM(point) AS point FROM tb_user_bpoint WHERE owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row["point"];
            }
        }
        return $return_result == "" ? 0 : $return_result;
    }

    function get_gpoint ($owner_ID)
    {
        $return_result = 0;
        $sql = "SELECT SUM(point) AS point FROM tb_user_gpoint WHERE owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row["point"];
            }
        }
        return $return_result == "" ? 0 : $return_result;
    }

    function get_all_reward ()
    {
        $return_result_arr = array();
        $sql = "SELECT tb_product.*, tb_bpoint.bpoint, tb_gpoint.gpoint
                FROM tb_product
                LEFT JOIN tb_bpoint ON tb_product.ID = tb_bpoint.product_ID
                LEFT JOIN tb_gpoint ON tb_product.ID = tb_gpoint.product_ID
                WHERE tb_product.status = '1' and type = '2'
                GROUP BY tb_product.ID;
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_g_reward ()
    {
        $return_result_arr = array();
        $sql = "SELECT tb_product.*, tb_bpoint.bpoint, tb_gpoint.gpoint
                FROM tb_product
                LEFT JOIN tb_bpoint ON tb_product.ID = tb_bpoint.product_ID
                LEFT JOIN tb_gpoint ON tb_product.ID = tb_gpoint.product_ID
                WHERE tb_product.status = '1' AND type = '2' AND tb_product.gpoint_exchange != '0'
                GROUP BY tb_product.ID;
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_b_reward ()
    {
        $return_result_arr = array();
        $sql = "SELECT tb_product.*, tb_bpoint.bpoint, tb_gpoint.gpoint
                FROM tb_product
                LEFT JOIN tb_bpoint ON tb_product.ID = tb_bpoint.product_ID
                LEFT JOIN tb_gpoint ON tb_product.ID = tb_gpoint.product_ID
                WHERE tb_product.status = '1' AND type = '2' AND tb_product.bpoint_exchange != '0'
                GROUP BY tb_product.ID;
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

/*     function get_my_reward ()
    {
        $return_result_arr = array();
        $sql = "SELECT tb_product.*, tb_bpoint.bpoint, tb_gpoint.gpoint
                FROM tb_product
                LEFT JOIN tb_bpoint ON tb_product.ID = tb_bpoint.product_ID
                LEFT JOIN tb_gpoint ON tb_product.ID = tb_gpoint.product_ID
                WHERE tb_product.status = '1' and type = '2'
                GROUP BY tb_product.ID;
                ";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    } */

    function get_my_notification ($ID)
    {
        $return_result_arr = array();
        $sql = "SELECT * FROM tb_noti WHERE receiver = '$ID'";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function insert_new_notificaiton ($msg, $action, $receiver)
    {
        $sql = "INSERT INTO tb_noti (msg, action, receiver) VALUES ('$msg', '$action', '$receiver');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function get_my_chat ($receiver_ID, $sender_ID)
    {
        $return_result_arr = array();
        $sql = "SELECT * FROM tb_chat WHERE (sender = '$sender_ID' AND receiver = '$receiver_ID') OR (receiver = '$sender_ID' AND sender = '$receiver_ID') ORDER BY created_date ASC;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_bankname_option ()
    {
        $return_result = "<option selected disabled>กรุณาเลือกธนาคาร</option>";
        $sql = "SELECT * FROM tb_bankname;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $bankName_ID = $row["bankName_ID"];
                $bankName_name = $row["bankName_name"];
                $return_result .= "<option value = \"$bankName_ID\">$bankName_name</option>";
            }
        }
        return $return_result;
    }

    function get_bankname_option_selected ($ID)
    {
        $return_result = "<option selected disabled>กรุณาเลือกธนาคาร</option>";
        $sql = "SELECT * FROM tb_bankname;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $bankName_ID = $row["bankName_ID"];
                $bankName_name = $row["bankName_name"];
                if ($ID != $bankName_ID)
                    $return_result .= "<option value = \"$bankName_ID\">$bankName_name</option>";
                else if ($ID == $bankName_ID)
                    $return_result .= "<option value = \"$bankName_ID\" selected>$bankName_name</option>";
            }
        }
        return $return_result;
    }

    function insert_bank_account ($bankname_ID, $account_no, $name)
    {
        $owner_ID = $_SESSION["userinfo_ID"];
        $sql = "INSERT INTO tb_bank_account (owner_ID, bankname_ID, account_no, name, created_by) VALUES ('$owner_ID', '$bankname_ID', '$account_no', '$name', '$owner_ID');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function get_bank_info ($ID)
    {
        $return_result_arr = array();
        $sql = "SELECT * FROM tb_bankname WHERE ID = '$ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_bank_account_info_from_bank_account_ID ($ID)
    {
        $return_result_arr = array();
        $sql = "SELECT tb_bank_account.*, tb_bankname.bankName_name AS bank_name FROM tb_bank_account LEFT JOIN tb_bankname ON tb_bank_account.bankname_ID = tb_bankname.bankName_ID WHERE tb_bank_account.ID = '$ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_bank_account_info_from_userinfo_ID ($owner_ID)
    {
        $return_result_arr = array();
        $sql = "SELECT tb_bank_account.*, tb_bankname.bankName_name AS bank_name FROM tb_bank_account LEFT JOIN tb_bankname ON tb_bank_account.bankname_ID = tb_bankname.bankName_ID WHERE tb_bank_account.owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function withdraw ($owner_ID, $value)
    {
        $sql = "INSERT INTO tb_cash (owner_ID, value, created_by) VALUES ('$owner_ID', '$value', '$owner_ID')";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function get_my_sum_cash ($owner_ID)
    {
        $return_result = 0;
        $sql = "SELECT SUM(value) AS val FROM tb_cash WHERE owner_ID = '$owner_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            if ($row = $result->fetch_assoc())
            {
                $return_result = $row["val"];
            }
        }
        $return_result = empty($return_result) ? 0 : $return_result;
        return $return_result;
    }

    function get_giver_business_status($userinfo_ID)
    {
        $return_result = 0;
        $sql = "SELECT ID FROM tb_giver_register WHERE owner_ID = '$userinfo_ID';";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
            $return_result = 1;
        return $return_result;
    }

    function regis_giver_business ($userinfo_ID)
    {
        $created_by = empty($_SESSION["userinfo_ID"]) ? "0" : $_SESSION["userinfo_ID"] ;
        $sql = "INSERT INTO tb_giver_register (owner_ID, created_by) VALUES ('$userinfo_ID', '$created_by');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function created_pkey($owner_ID, $operation)
    {
        $public_key = uniqid();
        $sql = "INSERT INTO tb_pkey (public_key, owner_ID, operation)";
    }

    function check_exist_value ($column_name, $tb_name, $column_key, $value_key)
    {
        $return_result = 0;
        $result = get_ID ($column_name, $tb_name, $column_key, $value_key);
        if ($result != "")
            $return_result = 1;
        return $return_result;
    }

    function insert_new_product_pic_info ($nameonserver, $name_original, $url)
    {
        $created_by = $_SESSION["userinfo_ID"];
        $sql = "INSERT INTO tb_product_pic (name_onserver, name_original, url, created_by) VALUES ('$nameonserver', '$name_original', '$url', '$created_by')";
        $result = mysqli_query($GLOBALS["con"], $sql);
        echo mysqli_error($GLOBALS["con"]);
        return $result;
    }

    function add_store($name)
    {
        $created_by = $_SESSION["userinfo_ID"];
        $sql = "INSERT INTO tb_store (name, created_by) VALUES('$name', '$created_by');";
        $result = mysqli_query($GLOBALS["con"], $sql);
        return $result;
    }

    function get_all_store ()
    {
        $return_result_arr = array();
        $sql = "SELECT * FROM tb_store;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                array_push($return_result_arr, $row);
            }
        }
        return $return_result_arr;
    }

    function get_store_option_from_arr ($arr)
    {
        $return_result = "<option selected disabled>เลือกร้านค้า</option>";
        $i = 0;
        while ($arr[$i]["ID"] != "")
        {
            $return_result .= "<option value = ".$arr[$i]["ID"].">".$arr[$i]["name"]."</option>";
            $i++;
        }
        return $return_result;
    }

    function check_voucher ($voucher_uid, $voucher_key)
    {
        $return_result = 0;
        $voucher_uid = cut_quote($voucher_uid);
        $voucher_key = cut_quote($voucher_key);
        $sql = "SELECT * FROM tb_voucher_usage WHERE uid = '$voucher_uid' AND key = '$voucher_key' LIMIT 1;";
        $result = mysqli_query($GLOBALS["con"], $sql);
        if ($result->num_rows > 0)
        {
            $return_result = 1;
        }
        return $return_result;
    }

    function list_to_array($list)
    {
        return explode(",", $list);
    }

    function array_to_list ($arr)
    {
        return implode(",", $arr);
    }

    function increase_one_quantity ($order, $owner_ID)
    {
        $my_cart = get_cart($owner_ID);
        $product_list = $my_cart[0]["product_ID_list"];
        $quantity_list = $my_cart[0]["quantity_list"];
        $price_list = $my_cart[0]["price_list"];
        $product_arr = list_to_array($product_list);
        $quantity_arr = list_to_array($quantity_list);
        $price_arr = list_to_array($price_list);
        
        if (empty($order))
            $order = 0;
            
        $i = 0;
        while ($product_arr[$i] != "")
        {
            if ($order == $i)
            {
                $quantity_arr[$i]++ ;
                $product_detail = get_product_by_ID ($product_arr[$i]);
                $price_arr[$i] = $product_detail[0]["price"]*$quantity_arr[$i];
            }
            $i ++;
        }
        
        $product_list = array_to_list ($product_arr);
        $quantity_list = array_to_list ($quantity_arr);
        $price_list = array_to_list ($price_arr);
        
        $result = update_cart ($owner_ID, $product_list, $quantity_list, $price_list);
        return $reuslt;
    }

    function decrease_one_quantity ($order, $owner_ID)
    {
        $my_cart = get_cart($owner_ID);
        $product_list = $my_cart[0]["product_ID_list"];
        $quantity_list = $my_cart[0]["quantity_list"];
        $price_list = $my_cart[0]["price_list"];
        $product_arr = list_to_array($product_list);
        $quantity_arr = list_to_array($quantity_list);
        $price_arr = list_to_array($price_list);

        if (empty($order))
            $order = 0;
            
        $i = 0;
        while ($product_arr[$i] != "")
        {
            if (($order == $i) && ($quantity_arr[$i] > 1))
            {
                $quantity_arr[$order]-- ;
                $product_detail = get_product_by_ID ($product_arr[$order]);
                $price_arr[$order] = $product_detail[0]["price"]*$quantity_arr[$order];
            }
            $i++;
        }

        $product_list = array_to_list ($product_arr);
        $quantity_list = array_to_list ($quantity_arr);
        $price_list = array_to_list ($price_arr);
        
        $result = update_cart ($owner_ID, $product_list, $quantity_list, $price_list);
        return $reuslt;
    }
?>