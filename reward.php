<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    $page_header = "Reward";
    //alert(($_SESSION["userinfo_ID"]));
    $userinfo = get_userinfo($_SESSION["userinfo_ID"]);
    $product_arr = get_all_products();
    $regis_status = $_GET["regis_status"] == "1" ? "แลกซื้อ" : "สมัครสมาชิก" ;
    $bpoint = get_bpoint($_SESSION["userinfo_ID"]);
    $gpoint = get_gpoint($_SESSION["userinfo_ID"]);

    $bpoint = !empty($bpoint) ? $bpoint : 0 ;
    $gpoint = !empty($gpoint) ? $gpoint : 0 ;
    
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

    <style>
        ion-icon {
        color: white;
        }
        * {box-sizing:border-box}

        /* Slideshow container */
        .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
        }

        /* Hide the images by default */
        .mySlides {
        display: none;
        }

        /* slide_slide_text & slide_previous buttons */
        .slide_prev, .slide_slide_text {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        }

        /* Position the "slide_slide_text button" to the right */
        .slide_slide_text {
        right: 0;
        border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .slide_prev:hover, .slide_slide_text:hover {
        background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
        }

        .slide_active, .dot:hover {
        background-color: #717171;
        }

        /* Fading animation */
        .slide_fade {
        -webkit-animation-name: slide_fade;
        -webkit-animation-duration: 1.5s;
        animation-name: slide_fade;
        animation-duration: 1.5s;
        }

        @-webkit-keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }

        @keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }
    </style>
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- Header -->
    <?php include 'section_materials/topbar.php';?>
    <!-- Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div class="section mt-3 mb-3">
            <div class="card text-white mb-2" style = "background-color: #17a2b8;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body pb-1">
                    <div class="row w-100">
                        <div class="col-4 text-center bg-white py-2" style = "border-radius: 10%;">
                            <img src = "img/materials/logogiver2.png" width= "50%">
                        </div>
                        <div class="col-8">
                            <div class = "row mt-1">
                                <div class = "col-sm-12 col-md-4 col-xl-4 text-left my-1">ชื่อ: <?=$userinfo[0]["name"]?></div>
                            </div>
                            <div class = "row">
                                <div class = "col-sm-12 col-md-4 col-xl-4 text-left my-1">รหัสคำเชิญ: <?=$userinfo[0]["username"]?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100">
                        <input type = "text" style = "display: none;" id = "invite_code" value = "<?=$userinfo[0]["username"]?>">
                        <div class = "col-sm-12 col-md-4 col-xl-4 text-left my-1 text-center">
                            <input type = "button" class = "btn btn-danger rounded" value = "คัดลอกรหัสคำเชิญ" onclick = "notification('copy_noti', 3000);Clipboard_CopyTo('<?=$userinfo[0]["username"]?>');">
                            <input type = "button" class = "btn btn-danger rounded" data-toggle="modal" data-target="#ModalListview"  value = "แชร์คำเชิญ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <div class="col-6">
                            <div class = "my-auto font-weight-bold text-dark text-center">คะแนนสะสม GP</div>
                            <div class = "my-auto font-weight-bold text-danger text-center">
                                <span style = "font-size: 20px;"><?=number_format($gpoint)?> GPoint</span>
                            </div>
                        </div>
                        <div class="col-6 my-auto font-weight-bold text-danger text-center">
                            <div class = "">
                                <input type = "button" class = "btn btn-danger rounded" value = "แลกซื้อ" onclick = "window.location.href='reward_g_exchange.php';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-3">
            <div class="card mb-2" style = "background-color: #d9d9d9;">
                <!-- <div class="card-header"></div> -->
                <div class="card-body text-center py-2">
                    <div class="row w-100">
                        <div class="col-6">
                            <div class = "my-auto font-weight-bold text-dark text-center">คะแนนสะสม BP</div>
                            <div class = "my-auto font-weight-bold text-danger text-center">
                                <span style = "font-size: 20px;"><?=number_format($bpoint)?> Bpoint</span>
                            </div>
                        </div>
                        <div class="col-6 my-auto font-weight-bold text-danger text-center">
                            <div class = "">
                            <input type = "button" class = "btn btn-danger rounded" value = "แลกซื้อ" onclick = "window.location.href='reward_b_exchange.php';"><!--setting.php -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
        <?php include 'section_materials/bottom_menu_2.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

<!-- Modal Listview -->
<div class="modal fade modalbox" id="ModalListview" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">แชร์</h5>
                    <a href="javascript:;" data-dismiss="modal">ปิด</a>
                </div>
                <div class="modal-body p-0">

                    <ul class="listview image-listview flush mb-2">
                        <li onclick = "share_fb()">
                            <div class="item">
                                <img src="img/logo/facebook_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Facebook</div>
                                </div>
                            </div>
                        </li>
                        <li onclick = "share_line()">
                            <div class="item">
                                <img src="img/logo/line_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Line</div>
                                </div>
                            </div>
                        </li><!-- 
                        <li onclick = "copyToClipboard()">
                            <div class="item">
                                <img src="img/logo/link_icon.png" alt="image" class="image">
                                <div class="in">
                                    <div>คัดลอกลิงก์</div>
                                </div>
                            </div>
                        </li> -->
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div id="copy_noti" class="notification-box">
        <div class="notification-dialog ios-style">
            <div class="notification-header">
                <div class="in">
                    <!-- <img src="assets/img/sample/avatar/avatar3.jpg" alt="image" class="imaged w24 rounded"> -->
                    <strong>การแจ้งเตือน</strong>
                </div>
                <div class="right">
                    <span>just now</span>
                    <a href="#" class="close-button">
                        <ion-icon name="close-circle"></ion-icon>
                    </a>
                </div>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">คัดลอกลิงก์เรียบร้อยแล้ว</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- welcome notification  -->
    <!-- <div id="notification-welcome" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="assets/img/icon/72x72.png" alt="image" class="imaged w24">
                    <strong>Mobilekit</strong>
                    <span>just now</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Welcome to Mobilekit</h3>
                    <div class="text">
                        Mobilekit is a PWA ready Mobile UI Kit Template.
                        Great way to start your mobile websites and pwa projects.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- * welcome notification -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


    <script>
        setTimeout(() => {
            notification('notification-welcome', 5000);
        }, 2000);
    </script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        // slide_slide_text/slide_previous controls
        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" slide_active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " slide_active";
        }

        function show_load ()
        {
            
            async function main_func() 
            {
                // task 1
                document.getElementById('loader').style.display = '';
                
                // tast 2
                const result = await new Promise (resolve => {$("#loader").fadeToggle(500); resolve(1);});
            }

            main_func();
        }
        
        function Clipboard_CopyTo(value) {
        var tempInput = document.createElement("input");
        tempInput.value = 'https://www.giverapps.com/signup_selection.php?invite_code='+value;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
        alert("คัดลอกรหัสคำเชิญเรียบร้อยแล้ว");
        }

        document.querySelector('#Copy').onclick = function() {
        Clipboard_CopyTo('Text to copy!');
        }

        function share_fb()
        {
           /*  window.open(
                        'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
        function share_line()
        {
           /*  window.open(
                        'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
    </script>
</body>

</html>