<?php
    include '../connect.php';
    include '../main_function.php';
    session_start();
/*     if (empty($_GET["voucher_uid"]))
    {
        alert("ไม่มีรหัส Voucher");
        exit(0);
    } */
    if (empty($_GET["voucher_uid"]) || empty($_GET["voucher_key"]))
    {
      alert("รหัส Voucher ไม่ถูกต้อง (1)");
      exit(0);
    }
    else
    {
      //alert("มีรหัส Voucher ".$_GET["voucher_uid"]);
    }
    if (!check_voucher($_GET["voucher_uid"], $_GET["voucher_key"]))
    {
      alert("รหัส Voucher ไม่ถูกต้อง (2)");
      exit(0);
    }

    $voucher_arr = get_voucher_info ($_GET["voucher_uid"]);
    if (($voucher_arr == array()) || ($voucher_arr[0]["status"] == "1"))
    {
      alert("รหัส Voucher ไม่ถูกต้องหรือถูกใช้งานแล้ว");
      exit(0);
    }
?>
<!doctype html>
<html lang="en">

<head>
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?//=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?//=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?//=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json"> -->
    <?php include 'main_material/header.php'; ?>
    <style>
        .fa {
        color: red;
        }
        * {box-sizing:border-box}
    </style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
      <?php include 'main_material/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
          <?php include 'main_material/topbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <form action = "../frontend_operation.php" method = "POST" name = "use_voucher_form" id = "use_voucher_form">
            <div class = "row my-1" style = "display: none;">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    uid : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "uid" id = "uid"  value= "<?=$_GET["voucher_uid"]?>" placeholder = "" readonly>
                    <input type = "text" class = "form-control" name = "operation_command" id = "operation_command"  value= "use_voucher" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    วันที่ซื้อ : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "purchased_date" id = "purchased_date" value= "<?=$voucher_arr[0]["edited_date"]?>" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    เลขที่ออเดอร์ : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "voucher_usage_ID" id = "voucher_usage_ID" value= "<?=$voucher_arr[0]["ID"]?>" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    ชื่อผู้ซื้อ : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "owner_ID" id = "owner_ID" value= "<?=$voucher_arr[0]["name"]." ".$voucher_arr[0]["lastname"]?>" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    เบอร์ติดต่อ : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "tel" id = "tel" value= "<?=$voucher_arr[0]["username"]?>" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    รายละเอียด Voucher : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <textarea class = "form-control" name = "product_detail" id = "product_detail"  placeholder = "" readonly><?=$voucher_arr[0]["description"]?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    Gpoint : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "gpoint" id = "gpoint" value= "<?=empty($voucher_arr[0]["gpoint_exchange"]) ? 0 : $voucher_arr[0]["gpoint_exchange"] ?>" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    Bpoint : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "bpoint" id = "bpoint" value= "<?=empty($voucher_arr[0]["bpoint_exchange"]) ? 0 : $voucher_arr[0]["bpoint_exchange"] ?>" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1">
              <div class = "col-12 text-left">
                <div class = "row mb-1">
                  <div class = "col-12 text-left">
                    สถานะ : 
                  </div>
                </div>
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "text" class = "form-control" name = "purchase_status" id = "purchase_status" value= "ชำระแล้ว" placeholder = "" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class = "row my-1 mt-3">
              <div class = "col-12 text-left">
                <div class = "row">
                  <div class = "col-12 text-left">
                    <input type = "button" class = "btn btn-danger btn-block rounded-pill" name = "voucher_usage_ID" id = "voucher_usage_ID" value= "ยืนยันการใช้งาน" onclick = "document.getElementById('use_voucher_form').submit();">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php include 'main_material/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include 'main_material/modal.php'; ?>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script>
    function getXMLHTTP() 
    {
      var x = false;
      try {
        x = new XMLHttpRequest();
      }catch(e) {
        try {
          x = new ActiveXObject("Microsoft.XMLHTTP");
        }catch(ex) {
          try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
          }
          catch(e1) {
            x = false;
          }
        }
      }
      return x;
    }

    function findZone(val,get_value, push_ID)
    {
      var strURL = "ajax_request/frontend_operation.php";
      var req = getXMLHTTP();
      if (req)
      {
        req.onreadystatechange = function() 
        {
            if (req.readyState == 4) 
            {
              // only if "OK"
              if (req.status == 200) 
              {						
                  document.getElementById(push_ID).innerHTML=req.responseText;		
              } 
              else 
              {
                  alert("Problem while using XMLHTTP:\n" + req.statusText);
              }
            }
        }			
        req.open("POST", strURL, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send("operation_command=get_zone&selected_value="+get_value+"&search_word="+val);
      }		
    }

    function getUser(val)
    {
      var strURL = "ajax_request/frontend_operation.php";
      var req = getXMLHTTP();
      if (req)
      {
        req.onreadystatechange = function() 
        {
            if (req.readyState == 4) 
            {
              // only if "OK"
              if (req.status == 200) 
              {						
                  document.getElementById('userinfo_ID').innerHTML=req.responseText;	
                  console.log(req.responseText);	
              } 
              else 
              {
                  alert("Problem while using XMLHTTP:\n" + req.statusText);
              }
            }
        }			
        req.open("POST", strURL, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send("operation_command=get_user&search_word="+val);
      }		
    }

    function getFullname(value, push_name_ID, push_lastname_ID)
    {
        //suggest_userinfo_name;
        var strURL = "ajax_request/frontend_operation.php";
        var req = getXMLHTTP();
        if (req)
        {
            req.onreadystatechange = function() 
            {
                if (req.readyState == 4) 
                {
                // only if "OK"
                if (req.status == 200) 
                {						
                    //alert(req.responseText);		
                    var fullname = JSON.parse(req.responseText);
                    document.getElementById(push_name_ID).value = fullname.userinfo_name;
                    document.getElementById(push_lastname_ID).value = fullname.userinfo_lastname;
                } 
                else 
                {
                    alert("Problem while using XMLHTTP:\n" + req.statusText);
                }
                }
            }			
            req.open("POST", strURL, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send("operation_command=get_fullname&value="+value);
        }
    }
  </script>

</body>

</html>