<?php
    include '../connect.php';
    include '../main_function.php';
    session_start();
    if (empty($_GET["store_ID"]))
    {
        //alert("ไม่มีรหัสร้านค้า");
        //exit(0);
    }
    else
    {
      //alert("มีรหัส Voucher ".$_GET["voucher_uid"]);
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
      $where = "";
      $where .= empty($_POST["voucher_ID"]) ? "" : " AND tb_voucher_usage.uid = '".$_POST["voucher_ID"]."' " ;
      $where .= empty($_POST["voucher_name"]) ? "" : " AND tb_product.name LIKE '%".$_POST["voucher_name"]."%' " ;

      if ((!empty($_POST["date_start"])) && (!empty($_POST["date_end"])))
        $where .= " AND (tb_voucher_usage.purchase_date >= '".$_POST["date_start"]."%' AND tb_voucher_usage.purchase_date <= '".$_POST["date_end"]."%' + INTERVAL 1 DAY)";
      else if ((!empty($_POST["date_start"])) && (empty($_POST["date_end"])))
        $where .= " AND (tb_voucher_usage.purchase_date >= '".$_POST["date_start"]."%' + INTERVAL 1 DAY)";
      else if ((empty($_POST["date_start"])) && (!empty($_POST["date_end"])))
        $where .= " AND (tb_voucher_usage.purchase_date <= '".$_POST["date_end"]."%' + INTERVAL 1 DAY)";
      $unuse_voucher_arr = get_unuse_voucher_by_store_ID_where($_POST["store_ID"], $where);
      //print_r($unuse_vouchaer_arr);
      $used_voucher_arr = get_used_voucher_by_store_ID_where($_POST["store_ID"], $where);
    }
    else
    {
      $unuse_voucher_arr = get_unuse_voucher_by_store_ID($_GET["store_ID"]);
      $used_voucher_arr = get_used_voucher_by_store_ID($_GET["store_ID"]);
    }
   
    //print_r($used_voucher_arr);
?>
<!doctype html>
<html lang="en">

<head>
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?//=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?//=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?//=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json"> -->
    <?php include 'main_material/header.php'; ?>
    <style>
        .fa {
        color: red;
        }
        * {box-sizing:border-box}
    </style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
      <?php include 'main_material/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
          <?php include 'main_material/topbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="">
            <form action = "usage_history.php" method = "POST" name = "search_voucher" id = "search_voucher">
              <div style = "display: none;">
                <input type = "text" name = "store_ID" id = "store_ID" value = "<?=!empty($_GET["store_ID"]) ? $_GET["store_ID"] : $_POST["store_ID"]?>">
              </div>
              <div class="card shadow mb-4">
                <div class = "card-header">
                  ค้นหา vouchder
                </div>
                <div class = "card-body">
                  <div class = "row my-1">
                    <div class = "col-3">
                      รหัส Voucher
                      <input  type = "text" class = "form-control"
                              name = "voucher_ID" id = "voucher_ID"
                              value = "" placeholder = "">
                    </div>
                    <div class = "col-3">
                      ชื่อ Voucher
                      <input  type = "text" class = "form-control"
                              name = "voucher_name" id = "voucher_name"
                              value = "" placeholder = "">
                    </div>
                    <div class = "col-3">
                      วันที่ซื้อ Voucher (เริ่ม)
                      <input  type = "date" class = "form-control"
                              name = "date_start" id = "date_start"
                              value = "" placeholder = "">
                    </div>
                    <div class = "col-3">
                      วันที่ซื้อ Voucher (สิ้นสุด)
                      <input  type = "date" class = "form-control"
                              name = "date_end" id = "date_end"
                              value = "" placeholder = "">
                    </div>
                  </div>
                </div>
                <div class = "card-footer">
                  <div class = "row text-right">
                    <div class = "col">
                        <input type = "button" class = "btn btn-primary" value = "ค้นหา" onclick = "document.getElementById('search_voucher').submit();">
                    </div>
                  </div>
                </div>
              </div>
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
              <div class="card-header">
                ข้อมูล Voucher ที่ใช้แล้ว
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th class = "text-center">ที่</th>
                        <th class = "text-center">รหัส Voucher</th>
                        <th class = "text-center">ชื่อ Voucher</th>
                        <th class = "text-center">ชื่อผู้ใช้ Voucher</th>
                        <th class = "text-center">หมายเลขโทรศัพท์</th>
                        <th class = "text-center">วันที่ซื้อ Voucher</th>
                        <th class = "text-center">วันที่ใช้ Voucher</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      $i = 0;
                      $j = 1;
                      while ($used_voucher_arr[$i]["ID"] != "")
                      {
                    ?>
                      <tr>
                        <td class = "text-center"><?=$j?></td>
                        <td class = "text-center"><?=$used_voucher_arr[$i]["uid"]?></td>
                        <td class = "text-center"><?=$used_voucher_arr[$i]["product_name"]?></td>
                        <td class = "text-center"><?=$used_voucher_arr[$i]["userinfo_name"]." ".$used_voucher_arr[$i]["userinfo_lastname"]?></td>
                        <td class = "text-center"><?=$used_voucher_arr[$i]["tel"]?></td>
                        <td class = "text-center"><?=$used_voucher_arr[$i]["purchase_date"]?></td>
                        <td class = "text-center"><?=$used_voucher_arr[$i]["use_date"]?></td>
                      </tr>
                    <?php
                        $j++;
                        $i++;
                      }
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
              <div class="card-header">
                ข้อมูล Voucher ที่ยังไม่ใช้
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th class = "text-center">ที่</th>
                        <th class = "text-center">รหัส Voucher</th>
                        <th class = "text-center">ชื่อ Voucher</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      $i = 0;
                      $j = 1;
                      /* echo "<pre>";
                      print_r($unuse_voucher_arr);
                      echo "</pre>"; */
                      while ($unuse_voucher_arr[$i]["ID"] != "")
                      {
                    ?>
                      <tr>
                        <td class = "text-center"><?=$j?></td>
                        <td class = "text-center"><?=$unuse_voucher_arr[$i]["uid"]?></td>
                        <td class = "text-center"><?=$unuse_voucher_arr[$i]["product_name"]?></td>
                      </tr>
                    <?php
                        $j++;
                        $i++;
                      }
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php include 'main_material/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include 'main_material/modal.php'; ?>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script>
    function getXMLHTTP() 
    {
      var x = false;
      try {
        x = new XMLHttpRequest();
      }catch(e) {
        try {
          x = new ActiveXObject("Microsoft.XMLHTTP");
        }catch(ex) {
          try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
          }
          catch(e1) {
            x = false;
          }
        }
      }
      return x;
    }

    function findZone(val,get_value, push_ID)
    {
      var strURL = "ajax_request/frontend_operation.php";
      var req = getXMLHTTP();
      if (req)
      {
        req.onreadystatechange = function() 
        {
            if (req.readyState == 4) 
            {
              // only if "OK"
              if (req.status == 200) 
              {						
                  document.getElementById(push_ID).innerHTML=req.responseText;		
              } 
              else 
              {
                  alert("Problem while using XMLHTTP:\n" + req.statusText);
              }
            }
        }			
        req.open("POST", strURL, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send("operation_command=get_zone&selected_value="+get_value+"&search_word="+val);
      }		
    }

    function getUser(val)
    {
      var strURL = "ajax_request/frontend_operation.php";
      var req = getXMLHTTP();
      if (req)
      {
        req.onreadystatechange = function() 
        {
            if (req.readyState == 4) 
            {
              // only if "OK"
              if (req.status == 200) 
              {						
                  document.getElementById('userinfo_ID').innerHTML=req.responseText;	
                  console.log(req.responseText);	
              } 
              else 
              {
                  alert("Problem while using XMLHTTP:\n" + req.statusText);
              }
            }
        }			
        req.open("POST", strURL, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send("operation_command=get_user&search_word="+val);
      }		
    }

    function getFullname(value, push_name_ID, push_lastname_ID)
    {
        //suggest_userinfo_name;
        var strURL = "ajax_request/frontend_operation.php";
        var req = getXMLHTTP();
        if (req)
        {
            req.onreadystatechange = function() 
            {
                if (req.readyState == 4) 
                {
                // only if "OK"
                if (req.status == 200) 
                {						
                    //alert(req.responseText);		
                    var fullname = JSON.parse(req.responseText);
                    document.getElementById(push_name_ID).value = fullname.userinfo_name;
                    document.getElementById(push_lastname_ID).value = fullname.userinfo_lastname;
                } 
                else 
                {
                    alert("Problem while using XMLHTTP:\n" + req.statusText);
                }
                }
            }			
            req.open("POST", strURL, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send("operation_command=get_fullname&value="+value);
        }
    }
  </script>

</body>

</html>