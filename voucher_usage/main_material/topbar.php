        <?php
          $userinfo_ID = $_SESSION["userinfo_ID"];
          $sql_get_fullname = "SELECT userinfo_name, userinfo_lastname FROM tb_userinfo WHERE userinfo_ID = '$userinfo_ID';";
          $result_get_fullname = mysqli_query($con, $sql_get_fullname);
          $fullname_topbar = "";
          if ($result_get_fullname->num_rows > 0)
          {
            if ($row = $result_get_fullname->fetch_assoc())
            {
              $fullname_topbar = $row["userinfo_name"]." ".$row["userinfo_lastname"];
            }
          }
        ?>
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <div class = "row w-100">
            <div class = "col-12 text-center weight-font-bold text-dark">
                Voucher manager
            </div>
          </div>
        </nav>