<?php
    include 'connect.php';
    include 'main_function.php';
    session_start ();
    if (!empty($_SESSION[""]))
        echo "<script>window.location.replace('index.php')</script>";
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (!empty($_POST["tel"]) && !empty($_POST["otp"]))
        {
            if (!empty($_POST["invite_code"]))
                $invite_code = $_POST["invite_code"];
            else
                $invite_code = "";
            $username = $_POST["tel"];
            $otp = ($_POST["otp"]);
            $result_check_otp = check_otp($username, $otp);
            if (!($result_check_otp))
            {
                alert();
                alertBack("ไม่สามารถยืนยัน OTP ได้ กรุณาลองใหม่อีกครั้ง");
            }
            else
            {
                $_SESSION["username"] = $username;
                jsRedirect("replace", "invite_code.php?invite_code=$invite_code");
            }
        }
        else
            jsBack();
    }
    exit(0);
?>