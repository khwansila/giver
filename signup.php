<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (!empty($_SESSION["userinfo_ID"]))
        jsRedirect("replace", "index.php");
    if (!empty($_POST["invite_code"]))
        $invite_code = $_POST["invite_code"];
    else
        $invite_code = "";
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body style = " background-image: url('img/materials/giver_background.png');
                background-repeat:no-repeat;
                background-size:cover;">

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->


    <!-- App Capsule -->
    <div id="appCapsule" class="pt-0">

        <div class="login-form mt-5 pt-5">
            <div class="section">
                <img src="img/materials/GIVER-LOGO-PNG.png" alt="image" class="form-image">
            </div>
            <div class="section mt-1">
                <h1>Welcome to Giver</h1>
                <!-- <h4>Fill the form to log in</h4> -->
            </div>
            <div class="section mt-1 mb-5">
                <form action="check_signup.php" method = "POST">
                    <div style = "display: none;">
                        <input type = "text" name = "invite_code" value = "<?=$invite_code?>">
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" style = "border-radius: 35px;" class="form-control" name = "tel" id="tel" placeholder="Mobile number" onkeyup = "check_password();check_exist_username ();">
                            <i class="clear-input">
                                <ion-icon name="close-circle" onclick = "check_password();hide_submit_div ();"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" style = "border-radius: 35px;" class="form-control" name = "password1" id="password1" placeholder="Password" onkeyup = "check_password()">
                            <i class="clear-input">
                                <ion-icon name="close-circle" onclick = "check_password();hide_submit_div ();"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" style = "border-radius: 35px;" class="form-control" name = "password2" id="password2" placeholder="Re password" onkeyup = "check_password()">
                            <i class="clear-input">
                                <ion-icon name="close-circle" onclick = "check_password();hide_submit_div ();"></ion-icon>
                            </i>
                        </div>
                    </div>
                    
                    <div class="form-group boxed" style = "display: none;" name = "username_exist" id = "username_exist">
                        <div class="input-wrapper">
                            <div class  = "mx-auto text-red" > 
                                <span style = "color: red;">หมายเลขโทรศัพท์นี้ถูกใช้งานแล้ว</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group boxed" style = "display: none;" name = "password_match" id = "password_match">
                        <div class="input-wrapper">
                            <div class  = "mx-auto text-red"> 
                                <span style = "color: red;">กรุณาใส่หมายเลขโทรศัพท์มือถือ</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group boxed" style = "display: none;" name = "password_unmatch" id = "password_unmatch">
                        <div class="input-wrapper">
                            <div class  = "mx-auto text-red"> 
                                <span style = "color: red;">รหัสผ่านไม่ถูกต้อง</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-links mt-2" name = "submit_div" id = "submit_div" style = "display: none;">
                        <div class = "row w-100 mx-auto">
                            <button type="submit" class="btn btn-danger btn-block btn-lg" style = "border-radius: 35px;">Sign up</button>
                        </div>
                    </div>

                    <!-- <div class="form-links mt-2">
                        <div>
                            <a href="page-register.html">Register Now</a>
                        </div>
                        <div><a href="page-forgot-password.html" class="text-muted">Forgot Password?</a></div>
                    </div>

                    <div class="form-button-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Log in</button>
                    </div> -->

                </form>
            </div>
        </div>


    </div>
    <!-- * App Capsule -->

    <div style = "display: none;" id = "temp_div">
    </div>

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    <script src="assets/js/main_function.js"></script>

    <script>
            function check_password()
            {
                var password_1_val = document.getElementById('password1').value;
                var password_2_val = document.getElementById('password2').value;
                var tel_val = document.getElementById('tel').value;

                if ((tel_val == "") && (password_1_val == "") && (password_2_val == ""))
                {
                    document.getElementById('password_match').style.display = 'none';
                    document.getElementById('password_unmatch').style.display = 'none';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val == "") && (password_1_val == "") && (password_2_val != ""))
                {
                    document.getElementById('password_match').style.display = 'none';
                    document.getElementById('password_unmatch').style.display = '';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val == "") && (password_1_val != "") && (password_2_val == ""))
                {
                    document.getElementById('password_match').style.display = 'none';
                    document.getElementById('password_unmatch').style.display = '';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val == "") && (password_1_val != "") && (password_2_val != ""))
                {
                    document.getElementById('password_match').style.display = '';
                    document.getElementById('password_unmatch').style.display = 'none';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val != "") && (password_1_val == "") && (password_2_val == ""))
                {
                    document.getElementById('password_match').style.display = 'none';
                    document.getElementById('password_unmatch').style.display = '';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val != "") && (password_1_val == "") && (password_2_val != ""))
                {
                    document.getElementById('password_match').style.display = 'none';
                    document.getElementById('password_unmatch').style.display = '';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val != "") && (password_1_val != "") && (password_2_val == ""))
                {
                    document.getElementById('password_match').style.display = 'none';
                    document.getElementById('password_unmatch').style.display = '';
                    document.getElementById('submit_div').style.display = 'none';
                }
                else if ((tel_val != "") && (password_1_val != "") && (password_2_val != ""))
                {
                    if (password_1_val == password_2_val)
                    {
                        document.getElementById('password_match').style.display = 'none';
                        document.getElementById('password_unmatch').style.display = 'none';
                        document.getElementById('submit_div').style.display = '';
                    }
                    else
                    {
                        document.getElementById('password_match').style.display = 'none';
                        document.getElementById('password_unmatch').style.display = '';
                        document.getElementById('submit_div').style.display = 'none';
                    }
                    check_exist_username ();
                }
            }
            function hide_submit_div ()
            {
                document.getElementById('submit_div').style.display = 'none';
            }

            async function check_exist_username ()
            {
                var password_1_val = document.getElementById('password1').value;
                var password_2_val = document.getElementById('password2').value;
                var tel_val = document.getElementById('tel').value;
                result = ajax_req("ajax_request.php", "operation_command=check_exist_username&username="+tel_val, after_ajax);
                function after_ajax(result)
                {
                    if (result == "1")
                    {
                        document.getElementById('username_exist').style.display = '';
                        document.getElementById('submit_div').style.display = 'none';
                    }
                    else if (result != "1")
                    {
                        if ((password_1_val != "") && (password_2_val != "") && (password_1_val == password_2_val) && (tel_val != ""))
                        {
                            document.getElementById('username_exist').style.display = 'none';
                            document.getElementById('submit_div').style.display = '';
                        }
                    }
                }
                
                //const new_result = await new Promise (resolve => {after_ajax(1);resolve(result);});
                //alert(document.getElementById("temp_div").innerHTML);
                //alert(result);
            }
    </script>
</body>

</html>