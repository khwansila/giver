<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (!empty($_POST["operation_command"]))
        {
            $operation_command = $_POST["operation_command"];
        }

        if ($operation_command == "reset_password")
        {
            if (!empty($_POST["tel"]) && !empty($_POST["otp"]))
            {
                $check_otp_result = check_otp ($_POST["tel"], $_POST["otp"]);
                if ($check_otp_result)
                {
                    $_SESSION["reset_password_username"] = $_POST["tel"];
                    jsRedirect("replace", "change_password.php");
                }
                    
                else
                    alertBack("OTP ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
            }
            else
            {
                jsBack();
            }
        }
        else if ($operation_command == "set_new_password")
        {
            $username = $_SESSION["reset_password_username"];
            $password = md5($_POST["password1"]);
            $result_set_new_password = set_new_password($username, $password);
            if ($result_set_new_password)
            {
                $_SESSION["reset_password_username"] = "";
                alertGoto("เปลี่ยนรหัสผ่านเรียบร้อยแล้ว", "login.php");
            }
            else
                alertBack("ไม่สามารถเปลี่ยนรหัสผ่านได้ กรุณาลองใหม่อีกครั้ง");
        }
        else if ($operation_command == "create_order")
        {
            if (empty($_POST["refno"]) || empty($_POST["productdetail"]) || empty($_POST["total"]))
            {
                alertGoto("การชำระเงินมีปัญหา กรุณาติดต่อผู้ดูแลระบบ", "index.php");
            }
            $ref_no = $_POST["refno"];
            $owner_ID = $_POST["productdetail"];
            $purchased_price = $_POST["total"];

            $cart_arr = get_cart($owner_ID);
            $net_price = get_net_value_cart($owner_ID);
            
            $product_ID_list = $cart_arr[0]["product_ID_list"];
            $quantity_list = $cart_arr[0]["quantity_list"];
            $price_list = $cart_arr[0]["price_list"];

            $result_create_order = create_order ($owner_ID, $ref_no, $product_ID_list, $quantity_list, $price_list, $net_price, $purchased_price);
            if ($result_create_order)
            {
                $result_clear_order = clear_cart($owner_ID);
            }

            $product_ID_arr = explode(",", $cart_arr[0]["product_ID_list"]);
            $quantity_arr = explode(",", $cart_arr[0]["quantity_list"]);
            $i = 0;
            $b_point = 0;
            $g_point = 0;
            while ($product_ID_arr[$i] != "")
            {
				insert_error ("product_ID_arr=".$product_ID_arr[$i], "quantity_arr=".$quantity_arr[$i]);
                $b_point += calculate_b_point($product_ID_arr[$i], $quantity_arr[$i]);
                $g_point += calculate_g_point($product_ID_arr[$i], $quantity_arr[$i]);
                $i++;
            }
            insert_b_point_by_ref_no ($owner_ID, $b_point, $ref_no);
            insert_g_point_by_ref_no ($owner_ID, $g_point, $ref_no);
            jsRedirect("replace", "order.php");
        }
        else if ($operation_command == "edit_personal_info")
        {
            $owner_ID = $_SESSION["userinfo_ID"];
            $name = $_POST["name"];
            $lastname = $_POST["lastname"];
            $gender = $_POST["gender"];
            $birthdate = $_POST["birthdate"];
            update_userinfo ($owner_ID, $name, $lastname, $gender, $birthdate);
            jsRedirect("replace", "setting.php");
        }
        else if ($operation_command == "update_address")
        {
            if (!empty($_POST["address_detail"]) && !empty($_POST["zone_ID"]))
            {
                $owner_ID = $_SESSION["userinfo_ID"];
                $address_detail  = $_POST["address_detail"];
                $zone_ID = $_POST["zone_ID"];
            }
            else
            {
                alertBack("ไม่สามารถบันทึกที่อยู่ใหม่ได้ กรุณาลองใหม่อีกครั้ง");
                exit(0);
            }
            $current_address = get_address($owner_ID);
            if (empty($current_address))
                insert_address ($owner_ID, $address_detail, $zone_ID);
            else
                update_address ($owner_ID, $address_detail, $zone_ID);
            jsRedirect("replace", "setting.php");
        }
        else if ($operation_command == "add_product")
        {
            $barcode = $_POST["barcode"];
            $name = $_POST["name"];
            $description = $_POST["description"];
            $spec = $_POST["spec"];
            $price = $_POST["price"];
            $stock = $_POST["stock"];
            $result = add_product($barcode, $name, $description, $spec, $price, $stock);
            if (!($result))
                alertBack("ไม่สามารถบันทึกสินค้าได้ กรุณาลองใหม่อีกครั้ง");
            else
                alert("บันทึกสินค้าเรียบร้อยแล้ว");
            jsRedirect("replace", "back_office/add_product.php");
        }
        else if ($operation_command == "add_voucher")
        {
            $barcode = $_POST["barcode"];
            $name = $_POST["name"];
            $description = $_POST["description"];
            $spec = $_POST["spec"];
            $price = $_POST["price"];
            $stock = $_POST["stock"];
            $store_ID = $_POST["store_ID"];
            $result = add_voucher($barcode, $name, $description, $spec, $price, $stock, $store_ID);
            if (!($result))
                alertBack("ไม่สามารถบันทึกสินค้าได้ กรุณาลองใหม่อีกครั้ง");
            else
                alert("บันทึกสินค้าเรียบร้อยแล้ว");
            jsRedirect("replace", "product_manager/add_voucher.php");
        }
        else if ($operation_command == "use_voucher")
        {
            $uid = $_POST["uid"];
            use_voucher ($uid);
            $voucher_arr = get_voucher_info ($uid);
            //print_r($voucher_arr);
            jsRedirect("replace", "voucher_usage/usage_history.php?store_ID=".$voucher_arr[0]["store_ID"]);
        }
        else if ($operation_command == "exchange_voucher")
        {
            $product_ID = $_POST["product_ID"];
            $owner_ID = $_SESSION["userinfo_ID"];
            $owner_bpoint = get_bpoint ($owner_ID);
            $owner_gpoint = get_gpoint ($owner_ID);
            $bpoint_exchange = get_bpoint_exchange($product_ID);
            $gpoint_exchange = get_gpoint_exchange($product_ID);
            if (($owner_bpoint < $bpoint_exchange) || ($owner_gpoint < $gpoint_exchange))
            {
                alert("จำนวน point ไม่เพียงพอ");
            }
            else
            {
                if ($bpoint_exchange != 0)
                {
                    $point_type = "b";
                    $point_value = $bpoint_exchange;
                }
                else
                {
                    $point_type = "g";
                    $point_value = $gpoint_exchange;
                }
                $result = insert_owner_voucher ($_SESSION["userinfo_ID"], $product_ID, 1, $point_type, $point_value);
                if ($result)
                    alert("แลก Voucher เรียบร้อยแล้ว");
            }
            //alert("owner_bpoint:$owner_bpoint  bpoint_exchange:$bpoint_exchange  owner_gpoint:$owner_gpoint   bpoint_gxchange:$gpoint_exchange");
            //jsRedirect("replace", "reward_g_exchange.php");
            jsBack();
        }
        else if ($operation_command == "add_bank_account")
        {
            $bankname_ID = $_POST["bankname_ID"];
            $owner_name = $_POST["owner_name"];
            $account_no = $_POST["account_no"];

            $result_insert_bank_account = insert_bank_account ($bankname_ID, $account_no, $owner_name);
            /* if ($result_insert_bank_account)
            {

            } */
            $sql_get_last_bank_account_ID = "SELECT ID FROM tb_bank_account WHERE bankname_ID = '$bankname_ID' AND account_no = '$account_no' AND name = '$owner_name' ORDER BY edited_date DESC LIMIT 1;";
            $result = mysqli_query($GLOBALS["con"], $sql_get_last_bank_account_ID);
            if ($result->num_rows > 0)
            {
                if ($row = $result->fetch_assoc())
                {
                    $bank_account_ID = $row["ID"];
                }
            }
            jsRedirect("replace", "add_bank_4.php?bank_account_ID=".$bank_account_ID);
        }
        else if ($operation_command == "withdraw")
        {
            $owner_ID = $_SESSION["userinfo_ID"];
            $value = $_POST["value"]*-1;
            $result = withdraw ($owner_ID, $value);
            if ($result)
                alert("ถอนเงินสำเร็จ");
            jsRedirect("replace", "profile.php");
        }
        else if ($operation_command == "add_notification")
        {
            $owner_ID = $_SESSION["userinfo_ID"];
            
            $msg = $_POST["msg"];
            $action = $_POST["action"];
           
            $userinfo_list = get_userinfo_list ();
            $userinfo_arr = explode(",", $userinfo_list); 

            $i = 0;
            while ($userinfo_arr[$i] != "")
            {
                insert_new_notificaiton ($msg, $action, $userinfo_arr[$i]);
                $i++;
            }
            jsRedirect("replace", "product_manager/add_notification.php");
        }
    }
    exit(0);
?>