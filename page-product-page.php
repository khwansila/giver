<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    //alert(($_SESSION["userinfo_ID"]));
    if (!empty($_GET["product_ID"]))
    {
        $product_ID = $_GET["product_ID"];
    }
    else
        $product_ID = 1;
    $product_arr = get_product_by_ID ($product_ID);

?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-danger text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">รายละเอียดสินค้า</div>
        <!-- <div class="right">
            <a href="#" class="headerButton">
                <ion-icon name="star-outline"></ion-icon>
            </a>
        </div> -->
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div style = "display: none;">
            <input type = "text" name = "share_ID" id = "share_ID" value = "default">
        </div>

        <!-- carousel -->
        <div class="carousel-full owl-carousel owl-theme">
            <div class="item">
                <img src="product_pic/<?=$product_arr[0]["pic_url"]?>" alt="alt" class="imaged w-100 square">
            </div>
<!--             <div class="item">
                <img src="assets/img/sample/photo/product2.jpg" alt="alt" class="imaged w-100 square">
            </div>
            <div class="item">
                <img src="assets/img/sample/photo/product3.jpg" alt="alt" class="imaged w-100 square">
            </div> -->
        </div>
        <!-- * carousel -->
        <div class="section full">
            <div class="wide-block pt-2 pb-2 product-detail-header">
                <!-- <div class="rate-block mb-1">
                    <ion-icon name="star" class="active"></ion-icon>
                    <ion-icon name="star" class="active"></ion-icon>
                    <ion-icon name="star" class="active"></ion-icon>
                    <ion-icon name="star" class="active"></ion-icon>
                    <ion-icon name="star"></ion-icon>
                </div> -->
                <h1 class="title"><?=$product_arr[0]["name"]?></h1>
                <div class = "row"> 
                    <div class= "col-8">
                        <div class="text">
                            <div class = "row">
                                <div class = "col-6">
                                    <div class = "row rounded-pill" style = "background-color: #ADFEFE;">
                                        <div class = "col-2 m-1 p-0"> 
                                            <img src="img/badges/bp_badge.png" width = "auto" height = "auto" >
                                        </div>
                                        <div class = "col-4 my-auto"> 
                                            <h6 class = "my-0 py-0"><?=$product_arr[0]["bpoint"]?> บาท</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text">
                            <div class = "row">
                                <div class = "col-6">
                                    <div class = "row rounded-pill mt-1" style = "background-color: #FEADB3;">
                                        <div class = "col-2 m-1 p-0"> 
                                            <img src="img/badges/gp_badge.png" width = "auto" height = "auto" >
                                        </div>
                                        <div class = "col-4 my-auto"> 
                                            <h6 class = "my-0 py-0"><?=$product_arr[0]["gpoint"]?> Gpoint</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text">
                            <div class = "row">
                                <div class = "col-6">
                                    <div class = "row rounded-pill mt-1" style = "background-color: #FFD133;">
                                        <div class = "col-2 m-1 p-0"> 
                                            <img src="img/badges/coin_26.png" width = "auto" height = "auto" >
                                        </div>
                                        <div class = "col-4 my-auto"> 
                                            <h6 class = "my-0 py-0"><?=$product_arr[0]["gpoint"]?> บาท</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class = "col-4">
                        <div class = "row">
                            <div class = "col text-center">
                                <img    src = "img/materials/btn-share.png" class = "my-3 pt-1" width = "60%" height = "auto"
                                        data-toggle="modal" data-target="#ModalListview" onclick = "document.getElementById('share_ID').value = window.location.href;">
                                </div>
                        </div>
                    </div>
                </div>
                <div class="detail-footer">
                    <!-- price -->
                    <div class="price">
                        <!-- <div class="old-price">$ 74.99</div> -->
                        <div class="current-price"><?=$product_arr[0]["price"]?> บาท</div>
                    </div>
                    <!-- * price -->
                    <!-- amount -->
                    <div class="amount">
                        <div class="stepper stepper-secondary">
                            <a href="#" class="stepper-button stepper-down">-</a>
                            <input type="text" class="form-control" name = "quantity" id = "quantity" value="1" readonly/>
                            <a href="#" class="stepper-button stepper-up">+</a>
                        </div>
                    </div>
                    <!-- * amount -->
                </div>

                <div class="section full mt-2">
                    <div class="section-title">รายละเอียดสินค้า</div>
                    <div class="wide-block pt-2 pb-2">
                        <?=$product_arr[0]["description"]?>
                    </div>

                </div>

                <div class = "detail-footer px-1 text-center">
                    <div class = "text">
                        <div class="btn btn-success rounded-pill btn-block btn-lg my-1 p-1">
                            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                            แชท
                        </div>
                    </div>
                    <div class = "text">
                        <div class="btn btn-warning rounded-pill btn-block btn-lg my-1" onclick = "add_cart('<?=$product_ID?>')">
                            <ion-icon name="cart-outline"></ion-icon>
                            ใส่ตะกร้า
                        </div>
                    </div>
                    <div class = "text">
                        <div class="btn btn-danger rounded-pill btn-block btn-lg btn-block my-1 p-1" onclick = "add_cart(); window.location.href = 'page-cart.php';">
                            &nbsp; ซื้อสินค้า &nbsp; 
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- 
        <div class="section full mt-2">
            <div class="section-title">Product Details</div>
            <div class="wide-block pt-2 pb-2">
                <?=$product_arr[0]["description"]?>
            </div>

        </div> 
        -->
    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
    <?php include 'section_materials/bottom_menu_1.php';?>
    <!-- * App Bottom Menu -->


   <!-- App Sidebar -->
   <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- Modal Listview -->
    <div class="modal fade modalbox" id="ModalListview" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">แชร์</h5>
                    <a href="javascript:;" data-dismiss="modal">ปิด</a>
                </div>
                <div class="modal-body p-0">

                    <ul class="listview image-listview flush mb-2">
                        <li onclick = "share_fb()">
                            <div class="item">
                                <img src="img/logo/facebook_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Facebook</div>
                                </div>
                            </div>
                        </li>
                        <li onclick = "share_line()">
                            <div class="item">
                                <img src="img/logo/line_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Line</div>
                                </div>
                            </div>
                        </li>
                        <li onclick = "copyToClipboard()">
                            <div class="item">
                                <img src="img/logo/link_icon.png" alt="image" class="image">
                                <div class="in">
                                    <div>คัดลอกลิงก์</div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <!-- * Modal Listview -->
    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

    <script src="assets/js/main_function.js"></script>

    <script>
        async function add_cart ()
        {
            var quantity = document.getElementById('quantity').value;
            result = ajax_req("ajax_request.php", "operation_command=add_to_cart&owner_ID=<?=$_SESSION["userinfo_ID"]?>&product_ID="+<?=$product_ID?>+"&quantity="+quantity, after_ajax);
            function after_ajax(result)
            {
                if (result == "1")
                {
                    alert('ใส่สินค้าลงตะกร้าเรียบร้อยแล้ว');
                }
                else
                {
                    alert('ใส่สินค้าลงตะกร้าไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');
                }
                /* else if (result != "1")
                {
                    if ((password_1_val != "") && (password_2_val != "") && (password_1_val == password_2_val) && (tel_val != ""))
                    {
                        document.getElementById('username_exist').style.display = 'none';
                        document.getElementById('submit_div').style.display = '';
                    }
                } */
            }
        }
        function copyToClipboard() 
        {
            //const textToCopy = document.getElementById("currentURL").value
            /* const textToCopy = window.location.href;
            navigator.clipboard.writeText(textToCopy)
            .then(() => { alert(`คัดลอกลิงก์หน้าเว็บนี้เรียบร้อยแล้ว`) })
            .catch((error) => { alert(`ไม่สามารถคัดลอกลิงก์หน้าเว็บนี้ได้ ${error}`) }) */

            copyText = document.getElementById("share_ID");
            copyText.select();
            //copyText.setSelectionRange(0, 99999);
            alert(typeof(copyText));
            document.execCommand("copy");

            //copyText.select();
            //copyText.setSelectionRange(0, 99999); /* For mobile devices */
            /* Copy the text inside the text field */
            //document.execCommand("copy");
        }
        function share_fb()
        {
           /*  window.open(
                        'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
        function share_line()
        {
           /*  window.open(
                        'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
    </script>

</body>

</html>