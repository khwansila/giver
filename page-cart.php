<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    //alert(($_SESSION["userinfo_ID"]));
    $cart = get_cart ($_SESSION["userinfo_ID"]);
    $product_ID_list = $cart[0]["product_ID_list"];
    $product_ID_arr = explode(",", $cart[0]["product_ID_list"]);
    $product_detail_arr = get_product_detail_from_list ($product_ID_list);
    $quantity_arr = explode(",", $cart[0]["quantity_list"]);
    $price_arr = explode(",", $cart[0]["price_list"]);
    $val = rand(1,99999999);
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-danger text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">ตะกร้าสินค้า</div>
        <!-- <div class="right">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#DialogClear">
                <ion-icon name="trash-outline"></ion-icon>
            </a>
        </div> -->
    </div>
    <!-- * App Header -->

    <!-- Dialog Clear -->
    <div class="modal fade dialogbox" id="DialogClear" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Empty Cart</h5>
                </div>
                <div class="modal-body">
                    All items will be deleted.
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CLOSE</a>
                        <a href="#" class="btn btn-text-danger" data-dismiss="modal">DELETE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * Dialog Clear -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <form method="post" action="https://www.thaiepay.com/epaylink/payment.aspx">
            
            <div class="section mt-2">
                <?php
                $net_price = 0;
                $i = 0;
                $j = 1;
                while (($product_ID_arr[$i] != ""))
                {
                ?>
                <!-- item -->
                <div class="card cart-item mb-2" id = "product_<?=$i?>">
                    <div class="card-body">
                        <div class="in">
                            <img src="product_pic/<?=$product_detail_arr[$i]["pic_url"]?>" alt="product" class="imaged">
                            <div class="text">
                                <h3 class="title"><?=$product_detail_arr[$i]["name"]?></h3>
                                <!-- <p class="detail"> <?//=$quantity_arr[$i]?> ชิ้น</p> -->
                                <strong class="price"><?=number_format($price_arr[$i])?> บาท</strong>
                            </div>
                        </div>
                        <div class="cart-item-footer">
                            <div class="stepper stepper-sm stepper-secondary">
                                <a href="#" class="stepper-button stepper-down" onclick = "decrease_quantity('<?=$i?>', 'quantity_<?=$j?>')">-</a>
                                <input  type="text" class="form-control" 
                                        name = "quantity_<?=$j?>" id = "quantity_<?=$j?>" 
                                        value="<?=$quantity_arr[$i]?>" disabled />
                                <a href="#" class="stepper-button stepper-up" onclick = "increase_quantity ('<?=$i?>', 'quantity_<?=$j?>')">+</a>
                            </div>
                            <a href="#" onclick = "delete_cart_item('<?=$j?>','product_<?=$i?>')" class="btn btn-outline-danger btn-sm">ลบ</a>
                            <!-- <a href="#" class="btn btn-outline-secondary btn-sm">Save it for later</a> -->
                        </div>
                    </div>
                </div>
                <!-- * item -->
                <?php
                $net_price += $price_arr[$i];
                $i++;
                $j++;
                }
                ?>
            </div>

            <div class="section mt-2 mb-2">
                <div class="card">
                    <ul class="listview flush transparent simple-listview">
                        <!-- <li>จำนวน (ชิ้น) <span class="text-muted">54.20</span></li> -->
                        <!-- <li>Shipping <span class="text-muted">฿ 2.90</span></li>
                        <li>Tax (10%)<span class="text-muted">฿ 5.70</span></li> -->
                        <li>ราคารวม (บาท)<span id = "net_price" class="text-danger font-weight-bold"><?=empty($net_price) ? 0 : number_format($net_price)?></span></li>
                    </ul>
                </div>
            </div>

            <input type="hidden" name="refno" value="<?=$val?>" />
            <input type="hidden" name="merchantid" value="66620580" />
            <input type="hidden" name="customeremail" value="pee.khwan@gmail.com" />
            <input type="hidden" name="cc" value="00" />
            <input type="hidden" name="productdetail" value="<?=$_SESSION["userinfo_ID"]?>" />
            <input type="hidden" name="total" id = "total" value="<?=empty($net_price) ? 0 : $net_price?>" />
            <div class="section mb-2">
                <!-- <a href="#" class="btn btn-danger btn-block btn-lg"></a> -->
                <input type = "submit" class = "btn btn-danger btn-block btn-lg" value = "ยืนยันการสั่งซื้อ">
            </div>
        </form>
    </div>
    <!-- * App Capsule -->

    <!-- App Bottom Menu -->
    <?php include 'section_materials/bottom_menu_4.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

    <script src="assets/js/main_function.js"></script>

    <script>
        async function delete_cart_item (position, product_position)
        {
            //var quantity = document.getElementById('quantity').value;
            result = ajax_req("ajax_request.php", "operation_command=delete_cart_item&owner_ID=<?=$_SESSION["userinfo_ID"]?>&position="+position, after_ajax);
            async function after_ajax(result, update_net_price)
            {
               if (result == "1")
                {
                    document.getElementById(product_position).style.display = "none";
                    ajax_req("ajax_request.php", "operation_command=update_net_price&owner_ID=<?=$_SESSION["userinfo_ID"]?>", update_net_price);
                }
                else
                {
                }
                function update_net_price (val)
                {
                    document.getElementById('net_price').innerHTML = val;
                    document.getElementById('total').value = val.replace(",","");
                    window.location.replace('page-cart.php');
                }
            }
           
        }
        async function increase_quantity (position, quantity_position)
        {
            //var quantity = document.getElementById('quantity').value;
            result = ajax_req("ajax_request.php", "operation_command=increase_quantity&owner_ID=<?=$_SESSION["userinfo_ID"]?>&order="+position, after_ajax);
            async function after_ajax(result, update_net_price)
            {
               if (result == "1")
                {
                    ajax_req("ajax_request.php", "operation_command=update_net_price&owner_ID=<?=$_SESSION["userinfo_ID"]?>", update_net_price);
                }
                else
                {
                }
                function update_net_price (val)
                {
                    console.log("val = "+val);
                    document.getElementById('net_price').innerHTML = val;
                    document.getElementById('total').value = val.replace(",","");
                }
            }
        }
        async function decrease_quantity (position, quantity_position)
        {
            //var quantity = document.getElementById('quantity').value;
            result = ajax_req("ajax_request.php", "operation_command=decrease_quantity&owner_ID=<?=$_SESSION["userinfo_ID"]?>&order="+position, after_ajax);
            async function after_ajax(result, update_net_price)
            {
               if (result == "1")
                {
                    ajax_req("ajax_request.php", "operation_command=update_net_price&owner_ID=<?=$_SESSION["userinfo_ID"]?>", update_net_price);
                }
                else
                {
                }
                function update_net_price (val)
                {
                    console.log("val = "+val);
                    document.getElementById('net_price').innerHTML = val;
                    document.getElementById('total').value = val.replace(",","");
                }
            }
        }
    </script>

</body>

</html>