<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    /* alert(empty($_SESSION["userinfo_ID"]) ? "empty" : "not empty");
    alert($_SESSION["userinfo_ID"]); */
    //alert("index session ".$_SESSION["userinfo_ID"]);
    $page_header = "Business Giver";
    $bankname_option = get_bankname_option ();

?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

    <style>
        * {box-sizing:border-box}

        /* Slideshow container */
        .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
        }

        /* Hide the images by default */
        .mySlides {
        display: none;
        }

        /* slide_slide_text & slide_previous buttons */
        .slide_prev, .slide_slide_text {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        }

        /* Position the "slide_slide_text button" to the right */
        .slide_slide_text {
        right: 0;
        border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .slide_prev:hover, .slide_slide_text:hover {
        background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
        }

        .slide_active, .dot:hover {
        background-color: #717171;
        }

        /* Fading animation */
        .slide_fade {
        -webkit-animation-name: slide_fade;
        -webkit-animation-duration: 1.5s;
        animation-name: slide_fade;
        animation-duration: 1.5s;
        }

        @-webkit-keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }

        @keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }
    </style>
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- Header -->
     <!-- App Header -->
     <div class="appHeader bg-danger text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"><?=$page_header?></div>
        <!-- <div class="right">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#DialogClear">
                <ion-icon name="trash-outline"></ion-icon>
            </a>
        </div> -->
    </div>
    <!-- * App Header -->
    <!-- Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div class = "section full mt-1 mb-1 text-center font-weight-bold text-dark">
            <h2>เพิ่มบัญชีธนาคาร</h2>
        </div>
        <hr class = "mb-0">
        <div class= "my-2" onclick = "">
            <form   action = "add_bank_3.php" method = "POST"
                    name = "add_bank_account_form" id = "add_bank_account_form">
                <div style = "display: none;">
                    <input type = "text" name = "operation_command" id = "operation_command" value = "add_bank_account">
                </div>
                <div class = "row px-2">
                    <div class = "col-12 text-left">
                        <div class = "mt-2 text-dark font-weight-bold">ชื่อ-นามสกุลที่ปรากฏในบัญชีธนาคาร</div>
                    </div>
                    <div class = "col-12 text-left">
                        <input  type = "text" class = "form-control" 
                                name = "owner_name" id = "owner_name">
                    </div>
                    <div class = "col-12 text-left">
                        <div class = "mt-2 text-dark font-weight-bold">เลขที่บัญชี</div>
                    </div>
                    <div class = "col-12 text-left">
                        <input  type = "text" class = "form-control" 
                                name = "account_no" id = "account_no">
                    </div>
                    <div class = "col-12 text-left">
                        <div class = "mt-2 text-dark font-weight-bold">ชื่อธนาคาร</div>
                    </div>
                    <div class = "col-12 text-left">
                        <select class = "form-control" name = "bankname_ID" id = "bankname_ID">
                            <?=$bankname_option?>;
                        </select>
                    </div>
                    <div class = "col-12 text-left mt-3">
                        <input  type = "button" class = "btn btn-danger btn-block" 
                                name = "" id = "" value = "ตรวจสอบข้อมูล"
                                onclick = "document.getElementById('add_bank_account_form').submit();">
                    </div>
                </div>
            </form>
        </div>
        <hr class = "m-0">


    </div>
    <!-- * App Capsule -->

    <!-- App Bottom Menu -->
     <?php include 'section_materials/bottom_menu_3.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    <script>
    </script>

</body>

</html>