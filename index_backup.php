<?php
    include 'connect.php';
    include 'main_function.php';
    session_start();
    if (empty($_SESSION["userinfo_ID"]))
       jsRedirect ("replace", "login.php");
    /* alert(empty($_SESSION["userinfo_ID"]) ? "empty" : "not empty");
    alert($_SESSION["userinfo_ID"]); */
    //alert("index session ".$_SESSION["userinfo_ID"]);
    $product_arr = get_all_products();
    $page_header = "หน้าแรก";

?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title><?=$GLOBALS["PROJECT_NAME"]?></title>
    <meta name="description" content="<?=$GLOBALS["PROJECT_DESCRIPTION"]?>">
    <meta name="keywords" content="<?=$GLOBALS["PROJECT_KEYWORD"]?>" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

    <style>
        * {box-sizing:border-box}

        /* Slideshow container */
        .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
        }

        /* Hide the images by default */
        .mySlides {
        display: none;
        }

        /* slide_slide_text & slide_previous buttons */
        .slide_prev, .slide_slide_text {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        }

        /* Position the "slide_slide_text button" to the right */
        .slide_slide_text {
        right: 0;
        border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .slide_prev:hover, .slide_slide_text:hover {
        background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
        }

        .slide_active, .dot:hover {
        background-color: #717171;
        }

        /* Fading animation */
        .slide_fade {
        -webkit-animation-name: slide_fade;
        -webkit-animation-duration: 1.5s;
        animation-name: slide_fade;
        animation-duration: 1.5s;
        }

        @-webkit-keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }

        @keyframes slide_fade {
        from {opacity: .4}
        to {opacity: 1}
        }
    </style>
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-danger" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- Header -->
    <?php include 'section_materials/topbar.php';?>
    <!-- Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        <div style = "display: none;">
            <input type = "text" name = "share_ID" id = "share_ID" value = "">
        </div>
        <div class="header-large-title">
            <h1 class="title">Welcome to Giver</h1>
            <h4 class="subtitle" style = "display: none">Welcome to Mobilekit</h4>
        </div>

        <div class="section full mt-3 mb-3">
            <div class="slideshow-container">

                <!-- Full-width images with number and caption text -->
                <div class="mySlides slide_fade">
                        <div class="numbertext"><!-- 1 / 3 --></div>
                            <img src="img/materials/1-1200x656.jpg" style="width: 100%;">
                        <div class="text"><!-- Caption Text --></div>
                </div>
            
                <div class="mySlides slide_fade">
                    <div class="numbertext"><!-- 2 / 3 --></div>
                    <img src="img/materials/2-1200x656.jpg" style="width: 100%;">
                    <div class="text"><!-- Caption Two --></div>
                </div>

                <div class="mySlides slide_fade">
                    <div class="numbertext"><!-- 2 / 3 --></div>
                    <img src="img/materials/flash_sale.jpg" style="width: 100%;">
                    <div class="text"><!-- Caption Two --></div>
                </div>
            
                <!-- <div class="mySlides slide_fade">
                    <div class="numbertext">3 / 3</div>
                    <img src="img3.jpg" style="width:100%">
                    <div class="text">Caption Three</div>
                </div> -->
            
                <!-- slide_slide_text and slide_previous buttons -->
                <a class="slide_prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="slide_slide_text" onclick="plusSlides(1)">&#10095;</a>
            </div>
        </div>

        <div class="section full mt-3 mb-3">
            <div class="carousel-multiple owl-carousel owl-theme">
                <?php
                $i = 0;
                while ($product_arr[$i]["ID"] != "")
                {
                ?>
                <div class="item">
                    <div class="card">
                        <div>
                            <img src="product_pic/<?=$product_arr[$i]["pic_url"]?>"  width = "320px" height = "auto" class="card-img-top" alt="image"  onclick = "window.location.href='page-product-page.php?product_ID=<?=$product_arr[$i]["ID"]?>'">
                            <div class="card-body pt-2">
                                <h4 class="mb-0"><?=$product_arr[$i]["name"]?></h4>
                                <div class = "row">
                                    <div class = "col-8"  onclick = "window.location.href='page-product-page.php?product_ID=<?=$product_arr[$i]["ID"]?>'">
                                        <div class = "row rounded-pill" style = "background-color: #ADFEFE;">
                                            <div class = "col-2 m-1 p-0"> 
                                                <img src="img/badges/bp_badge.png" width = "auto" height = "auto" >
                                            </div>
                                            <div class = "col-6 my-auto px-0"> 
                                                <h6 class = "my-0 py-0"><?=$product_arr[$i]["bpoint"]?> บาท</h6>
                                            </div>
                                        </div>
                                        <div class = "row rounded-pill mt-1" style = "background-color: #FEADB3;">
                                            <div class = "col-2 m-1 p-0"> 
                                                <img src="img/badges/gp_badge.png" width = "auto" height = "auto" >
                                            </div>
                                            <div class = "col-6 my-auto px-0"> 
                                                <h6 class = "my-0 py-0"><?=$product_arr[$i]["gpoint"]?> Gpoint</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "col-4">
                                        <img    src = "img/materials/btn-share.png" class = "my-3 pt-1" width = "16px" height = "auto"
                                                data-toggle="modal" data-target="#ModalListview" onclick = "document.getElementById('share_ID').value = '<?=$product_arr[$i]["ID"]?>'">
                                    </div>
                                </div>
                                <div class = "row pl-1 mt-1">
                                    <h5 class = " my-0">รับเงิน <span class = "text-danger"><?=$product_arr[$i]["commission_price"]?> บาท</span></h5>
                                </div>
                                <div class = "row pl-1 mt-1">
                                    <h5 class = "my-0">Cash back <span class = "text-danger"><?=$product_arr[$i]["commission_price"]?> บาท</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
                }
                ?>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card">
                <!-- <img src="assets/img/sample/photo/wide4.jpg" class="card-img-top" alt="image"> -->
                <div class="card-body">
                    <div class = "row text-center">
                        <div class = "col-3" onclick = "show_load()">
                            <img src="img/materials/catagories/menu_1.png" width = "auto">
                        </div>
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_2.png" width = "auto">
                        </div>
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_3.png" width = "auto">
                        </div>
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_4.png" width = "auto">
                        </div>
                    </div>
                    <div class = "row text-center">
                        <div class = "col-3">
                            <label>อาหารเสริม</label>
                        </div>
                        <div class = "col-3">
                            <label>เครื่องสำอาง</label>
                        </div>
                        <div class = "col-3">
                            <label>แม่และเด็ก</label>
                        </div>
                        <div class = "col-3">
                            <label>สินค้ายอดฮิต</label>
                        </div>
                    </div>
                    <div class = "row text-center">
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_5.png" width = "auto">
                        </div>
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_6.png" width = "auto">
                        </div>
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_7.png" width = "auto">
                        </div>
                        <div class = "col-3">
                            <img src="img/materials/catagories/menu_8.png" width = "auto">
                        </div>
                    </div>
                    <div class = "row text-center">
                        <div class = "col-3">
                            <label>อุปกรณ์ไอที</label>
                        </div>
                        <div class = "col-3">
                            <label>OTOP</label>
                        </div>
                        <div class = "col-3">
                            <label>ธุรกิจบริการ</label>
                        </div>
                        <div class = "col-3">
                            <label>สินค้าค่าคอมฯสูง</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card text-white bg-warning mb-2">
                <div class="card-header">
                    <img src = "img/materials/flash_sale.png" witdh = "auto">
                    <div class = "row"><div class = "col text-center mx-auto"id = "countdown"></div></div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">ลดสูงสุด 90% เฉพาะก่อนเที่ยงคืนเท่านั้น</h5>
                        <div class="row">
                            <?php
                                $i = 0;
                                while ($i < 2)
                                {
                            ?>
                            <div class="col-6">
                                <div class="card product-card">
                                    <div class="card-body">
                                        <img src="product_pic/<?=$product_arr[$i]["pic_url"]?>" class="image" alt="product image">
                                        <h2 class="title"><?=$product_arr[$i]["name"]?></h2>
                                        <div class="price"><?=$product_arr[$i]["price"]?> บาท</div>
                                        <a href="#" class="btn btn-sm btn-primary btn-block">ใส่ตะกร้า</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                                }
                            ?>
                        </div>
                </div>
            </div>
        </div>
        <div class="section mt-3 mb-1">
            <div class="card">
                <div class = "card-body rounded">
                    <img class = "rounded" src="img/materials/1-1200x656.jpg" style="width: 100%;">
                </div>
            </div>
        </div>

        <div class="section mt-1 mb-3">
            <div class = "row">
                <div class = "col-6">
                    <div class="card">
                        <div class = "card-body rounded">
                            <img class = "rounded" src="img/materials/2-1200x656.jpg" style="width: 100%;">
                        </div>
                    </div>
                </div>
                <div class = "col-6">
                    <div class="card">
                        <div class = "card-body rounded">
                            <img class = "rounded" src="img/materials/flash_sale.jpg" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="section mt-3 mb-3">
            <div class="card">
                <div class="card-body d-flex justify-content-between align-items-end">
                    <div>
                        <h6 class="card-subtitle">Discover</h6>
                        <h5 class="card-title mb-0 d-flex align-items-center justify-content-between">
                            Dark Mode
                        </h5>
                    </div>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input dark-mode-switch" id="darkmodeswitch">
                        <label class="custom-control-label" for="darkmodeswitch"></label>
                    </div>

                </div>
            </div>
        </div> -->

        <!-- <div class="section mt-3 mb-3">
            <div class="card">
                <img src="assets/img/sample/photo/wide4.jpg" class="card-img-top" alt="image">
                <div class="card-body">
                    <h6 class="card-subtitle">Discover</h6>
                    <h5 class="card-title">Components</h5>
                    <p class="card-text">
                        Reusable components designed for the mobile interface and ready to use.
                    </p>
                    <a href="app-components.html" class="btn btn-danger"> --> <!-- -->
                        <!-- <ion-icon name="cube-outline"></ion-icon>
                        Preview
                    </a>
                </div>
            </div>
        </div> -->

        <!-- <div class="section mt-3 mb-3">
            <div class="card">
                <img src="assets/img/sample/photo/wide2.jpg" class="card-img-top" alt="image">
                <div class="card-body">
                    <h6 class="card-subtitle">Discover</h6>
                    <h5 class="card-title">Pages</h5>
                    <p class="card-text">
                        Mobilekit comes with basic pages you may need and use in your projects easily.
                    </p>
                    <a href="app-pages.html" class="btn btn-danger"> --> <!-- -->
                        <!-- <ion-icon name="layers-outline"></ion-icon>
                        Preview
                    </a>
                </div>
            </div>
        </div> -->


        <!-- app footer -->
        <!-- <div class="appFooter">
            <img src="assets/img/logo.png" alt="icon" class="footer-logo mb-2">
            <div class="footer-title">
                Copyright © Mobilekit 2020. All Rights Reserved.
            </div>
            <div>Mobilekit is PWA ready Mobile UI Kit Template.</div>
            Great way to start your mobile websites and pwa projects.

            <div class="mt-2">
                <a href="javascript:;" class="btn btn-icon btn-sm btn-facebook">
                    <ion-icon name="logo-facebook"></ion-icon>
                </a>
                <a href="javascript:;" class="btn btn-icon btn-sm btn-twitter">
                    <ion-icon name="logo-twitter"></ion-icon>
                </a>
                <a href="javascript:;" class="btn btn-icon btn-sm btn-linkedin">
                    <ion-icon name="logo-linkedin"></ion-icon>
                </a>
                <a href="javascript:;" class="btn btn-icon btn-sm btn-instagram">
                    <ion-icon name="logo-instagram"></ion-icon>
                </a>
                <a href="javascript:;" class="btn btn-icon btn-sm btn-whatsapp">
                    <ion-icon name="logo-whatsapp"></ion-icon>
                </a>
                <a href="#" class="btn btn-icon btn-sm btn-secondary goTop">
                    <ion-icon name="arrow-up-outline"></ion-icon>
                </a>
            </div>

        </div> -->
        <!-- * app footer -->

    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
        <?php include 'section_materials/bottom_menu_1.php';?>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <?php include 'section_materials/sidebar.php';?>
    <!-- * App Sidebar -->

    <!-- welcome notification  -->
    <!-- <div id="notification-welcome" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img src="assets/img/icon/72x72.png" alt="image" class="imaged w24">
                    <strong>Mobilekit</strong>
                    <span>just now</span>
                </div>
                <a href="#" class="close-button">
                    <ion-icon name="close"></ion-icon>
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">Welcome to Mobilekit</h3>
                    <div class="text">
                        Mobilekit is a PWA ready Mobile UI Kit Template.
                        Great way to start your mobile websites and pwa projects.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- * welcome notification -->

    <!-- Modal Listview -->
    <div class="modal fade modalbox" id="ModalListview" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">แชร์</h5>
                    <a href="javascript:;" data-dismiss="modal">ปิด</a>
                </div>
                <div class="modal-body p-0">

                    <ul class="listview image-listview flush mb-2">
                        <li onclick = "share_fb()">
                            <div class="item">
                                <img src="img/logo/facebook_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Facebook</div>
                                </div>
                            </div>
                        </li>
                        <li onclick = "share_line()">
                            <div class="item">
                                <img src="img/logo/line_logo.png" alt="image" class="image">
                                <div class="in">
                                    <div>Line</div>
                                </div>
                            </div>
                        </li>
                        <li onclick = "copyToClipboard()">
                            <div class="item">
                                <img src="img/logo/link_icon.png" alt="image" class="image">
                                <div class="in">
                                    <div>คัดลอกลิงก์</div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!-- * Modal Listview -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    
    <script>
    var countDownDate = new Date("Jan 15, 2021 23:59:59").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();
        
    // Find the distance between now and the count down date
    var distance = countDownDate - now;
        
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
    // Output the result in an element with id="demo"
    document.getElementById("countdown").innerHTML = days + " วัน " + hours + "​ ชั่วโมง "
    + minutes + " นาที " + seconds + " วินาที ";
        
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
    }
    }, 1000);
    </script>

    <script>
        setTimeout(() => {
            notification('notification-welcome', 5000);
        }, 2000);
    </script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        // slide_slide_text/slide_previous controls
        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" slide_active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " slide_active";
        }

        function show_load ()
        {
            
            async function main_func() 
            {
                // task 1
                document.getElementById('loader').style.display = '';
                
                // tast 2
                const result = await new Promise (resolve => {$("#loader").fadeToggle(500); resolve(1);});
            }

            main_func();
        }
        function copyToClipboard() 
        {
            //const textToCopy = document.getElementById("currentURL").value
            const textToCopy = window.location.href+'/page-product-page.php?product_ID='+document.getElementById('share_ID').value;
            navigator.clipboard.writeText(textToCopy)
            .then(() => { alert(`คัดลอกลิงก์หน้าเว็บนี้เรียบร้อยแล้ว`) })
            .catch((error) => { alert(`ไม่สามารถคัดลอกลิงก์หน้าเว็บนี้ได้ ${error}`) })
        }
        function share_fb()
        {
           /*  window.open(
                        'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
        function share_line()
        {
           /*  window.open(
                        'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'),
                        'facebook-share-dialog', 
                        'width=626,height=436'); 
                        return false; */
                        window.location.href = 'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent('https://www.giverapps.com/signup_selection.php?invite_code=<?=$userinfo[0]["username"]?>'); 
        }
    </script>
</body>

</html>