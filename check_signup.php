<?php
    include 'connect.php';
    include 'main_function.php';
    session_start ();
    if (!empty($_SESSION[""]))
        echo "<script>window.location.replace('index.php')</script>";
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (!empty($_POST["tel"]) && !empty($_POST["password1"]))
        {
            if (!empty($_POST["invite_code"]))
                $invite_code = $_POST["invite_code"];
            else
                $invite_code = "";
            $username = $_POST["tel"];
            $password = md5($_POST["password1"]);
            $result_insert_new_user = insert_new_user ($username, $password);
            if (!($result_insert_new_user))
            {
                alertBack("ไม่สามารถสมัครสมาชิกได้ กรุณาลองใหม่อีกครั้ง");
            }
            else
            {
                jsRedirect("replace", "otp_request.php?tel=$username&invite_code=$invite_code");
            }
        }
        else
            jsBack();
    }
    exit(0);
?>